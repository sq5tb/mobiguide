package eu.ciastek.b.mobiguide

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import eu.ciastek.b.mobiguide.Singleton.currentGuidebook

internal class MapActivity : GuideActivity() {
    private var withPoints = true
    private var initialMoveAndZoom = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentViewWithToolbar(R.layout.activity_map, true)

        if (intent?.getBooleanExtra(IntentParam.WITH_POINTS, withPoints) == false) {
            withPoints = false
        }
    }


    override fun onResume() {
        super.onResume()
        if (withPoints) {
            val mMap = supportFragmentManager.findFragmentById(R.id.mapFragment)  as? MapFragment
            mMap?.setObject(currentGuidebook, moveTo = initialMoveAndZoom)
        }
        initialMoveAndZoom = false
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed(); true
            }
            R.id.action_settings -> {
                val i = Intent(this, SettingsActivity::class.java)
                i.action = PreferenceFragmentMap::class.java.canonicalName
                startActivity(i)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        if (withPoints) {
            menu?.addFilterIcon()
        }
        menu?.addSettings(true)
        return true
    }
}
