package eu.ciastek.b.mobiguide

import android.location.Location


internal class LatLong(
        var latitude: Double = Double.NaN,
        var longitude: Double = Double.NaN) {

    internal fun distanceTo(point: LatLong?): Float {
        return when {
            point == null -> Float.NaN
            this.isValid || point.isValid -> {
                val result = FloatArray(3)
                Location.distanceBetween(latitude, longitude, point.latitude, point.longitude, result)
                result.getOrElse(0) { Float.NaN }
            }
            else -> Float.NaN
        }
    }

    internal fun withinRadius(point: LatLong?, radius: Double): Boolean {
        return (distanceTo(point) <= radius)
    }

    internal val isValid: Boolean
        get() = (latitude >= -90.0 && latitude <= 90.0 && longitude >= -180.0 && longitude <= 360.0)

    companion object {
        internal fun create(latitude: Double?, longitude: Double?): LatLong? {
            return LatLong(latitude ?: Double.NaN, longitude ?: Double.NaN).takeIf { it.isValid }
        }
    }
}
