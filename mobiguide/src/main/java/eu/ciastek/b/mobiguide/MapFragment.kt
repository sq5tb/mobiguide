package eu.ciastek.b.mobiguide

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.*
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import eu.ciastek.b.mobiguide.Singleton.currentGuidebook
import org.mapsforge.map.android.graphics.AndroidGraphicFactory
import org.mapsforge.map.android.rendertheme.AssetsRenderTheme
import org.mapsforge.map.layer.renderer.MapWorkerPool
import org.mapsforge.map.reader.MapFile
import org.mapsforge.map.rendertheme.InternalRenderTheme
import org.mapsforge.map.rendertheme.XmlRenderTheme
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer
import org.osmdroid.bonuspack.kml.KmlDocument
import org.osmdroid.bonuspack.kml.Style
import org.osmdroid.config.Configuration
import org.osmdroid.events.ScrollEvent
import org.osmdroid.events.ZoomEvent
import org.osmdroid.mapsforge.MapsForgeTileProvider
import org.osmdroid.mapsforge.MapsForgeTileSource
import org.osmdroid.tileprovider.MapTileProviderArray
import org.osmdroid.tileprovider.MapTileProviderBasic
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.tileprovider.tilesource.bing.BingMapTileSource
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.*
import org.osmdroid.views.overlay.compass.CompassOverlay
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay
import org.osmdroid.views.overlay.infowindow.MarkerInfoWindow
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnShowRationale
import permissions.dispatcher.PermissionRequest
import permissions.dispatcher.RuntimePermissions
import splitties.toast.longToast
import splitties.toast.toast
import java.io.File
import java.util.*

internal typealias OnBubbleClickListener = (Context?, GuidePoi?) -> Unit

@RuntimePermissions
class MapFragment : Fragment(),
        SharedPreferences.OnSharedPreferenceChangeListener {

    private var onBubbleClickListener: OnBubbleClickListener = { c, poi ->
        c?.let {
            val i = Intent(it, DescriptionActivity::class.java)
            i.putExtra(GuidePoi::class.java.canonicalName.orEmpty(), poi?.uuid)
            startActivity(i)
        }
    }

    private var colorDisabled = Color.argb(255, 128, 128, 128)
    private val mapBorderSize = 50

    private val param = object {
        var mapZoom = -1.0
        var mapSource = String()
        var mapFile = String()
        var online = false
        var nightMode = false
        var renderTheme = String()
        var bingKey = String()
        var centerPoint: GeoPoint? = null
        var showOfflineWarning = true
        var followLocation = false
    }

    private val mFilteredCategoryTags = mutableSetOf<String>()
    private val mPoiMarkers = mutableListOf<Marker>()

    private var mMapView: MapView? = null
    private var mMapTileProvider: MapTileProviderArray? = null
    private var mLocationOverlay: MyLocationNewOverlay? = null
    private var mPoiCluster: RadiusMarkerClusterer? = null

    private var mService: GuideLocationService? = null
    private var mServiceIsConnected = false
    private val guideLocationConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as GuideLocationService.GuideLocationBinder
            mService = binder.service

            mMapView?.let { m ->
                mLocationOverlay = MyLocationNewOverlay(mService, m).apply {
                    enableMyLocation()
                    setFollowLocation()
                    isOptionsMenuEnabled = true
                    setDirectionArrow(
                        BitmapFactory.decodeResource(resources, R.drawable.person),
                        BitmapFactory.decodeResource(resources, R.drawable.next)
                    )
                }
                m.overlays.add(mLocationOverlay)
            }
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            mService = null
            mLocationOverlay?.apply {
                disableMyLocation()
                mMapView?.overlays?.remove(this)
            }
            mLocationOverlay = null
            setFollowLocation(false)
        }
    }


    private fun setFollowLocation(follow: Boolean = param.followLocation) {
        val button = view?.findViewById<ImageButton>(R.id.ic_follow_me)
        if (follow) {
            mLocationOverlay?.enableFollowLocation()

            if (mLocationOverlay?.isFollowLocationEnabled == true) {
                param.followLocation = true
                context?.let {c ->
                    val accent = ContextCompat.getColor(c, R.color.accent)
                    button?.setColorFilter(accent)
                }
            } else {
                param.followLocation = false
                button?.setColorFilter(colorDisabled)
                context?.toast(R.string.Can_not_enable_follow_location)
            }
        } else {
            mLocationOverlay?.disableFollowLocation()
            param.followLocation = false
            button?.setColorFilter(colorDisabled)
        }
    }

    override fun onAttach(c: Context) {
        super.onAttach(c)
        getParamsFromSharedPreferences(PreferenceManager.getDefaultSharedPreferences(c))

        Configuration.getInstance().load(c, PreferenceManager.getDefaultSharedPreferences(c))
        Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID
        Configuration.getInstance().save(c, PreferenceManager.getDefaultSharedPreferences(c))
        activity?.let {
            MapsForgeTileSource.createInstance(it.application)
            AndroidGraphicFactory.createInstance(it.application)
        }
        setupTileCacheWithPermissionCheck(c)
    }


    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun setupTileCache(c: Context?) {
        Configuration.getInstance().load(c, PreferenceManager.getDefaultSharedPreferences(c))
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_map, container, false)

        param.centerPoint = run {
            val p = context?.currentGuidebook?.center
            if (p?.isValid == true) GeoPoint(p.latitude, p.longitude) else null
        }

        mFilteredCategoryTags.clear()
        mFilteredCategoryTags.addAll(context.getFilteredCategoryTagsSet)
        addFilteredPoiClusterToMap()

        mMapView = view?.findViewById(R.id.mapview)
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mMapView?.apply {
            this.addMapListener(object : org.osmdroid.events.MapListener {
                override fun onScroll(event: ScrollEvent?): Boolean {
                    // disable follow location button status on map move
                    if (param.followLocation && mLocationOverlay?.isFollowLocationEnabled != true) {
                        setFollowLocation(false)
                    }
                    return true
                }

                override fun onZoom(event: ZoomEvent?): Boolean {
                    return false
                }
            })

            setUseDataConnection(false)

            zoomController.setVisibility(CustomZoomButtonsController.Visibility.SHOW_AND_FADEOUT)
            setMultiTouchControls(true)

            val mScaleBarOverlay = ScaleBarOverlay(this)
            mScaleBarOverlay.setAlignBottom(false)
            mScaleBarOverlay.setAlignRight(true)
            overlays.add(mScaleBarOverlay)

            val mCompassOverlay = CompassOverlay(context, InternalCompassOrientationProvider(context), this)
            mCompassOverlay.enableCompass()
            overlays.add(mCompassOverlay)
            overlays.add(CopyrightOverlay(context))

            val mRotationGestureOverlay = RotationGestureOverlay(this)
            mRotationGestureOverlay.isEnabled = true
            overlays.add(mRotationGestureOverlay)

            mPoiCluster = RadiusMarkerClusterer(context)
            mPoiCluster?.setIcon(BitmapFactory.decodeResource(resources, R.drawable.marker_cluster))
            overlays.add(mPoiCluster)

            if (param.mapZoom > 0.0) controller.setZoom(param.mapZoom)
            if (param.centerPoint is GeoPoint) controller.setCenter(param.centerPoint)
        }

        view.findViewById<ImageButton>(R.id.ic_center_position)?.apply {
            setOnClickListener {
                mService?.getLocation?.let { point ->
                    context?.toast(getString(R.string.my_position_is).format(point.latitude, point.longitude))
                    mMapView?.controller?.animateTo(GeoPoint(point.latitude, point.longitude))
                }
            }
            setOnLongClickListener {
                param.centerPoint?.let { center ->
                    context?.toast(getString(R.string.initial_map_center_is).format(center.latitude, center.longitude))
                    mMapView?.controller?.animateTo(center)
                }
                true
            }
        }

        view.findViewById<ImageButton>(R.id.ic_follow_me)?.apply {
            setOnClickListener {
                param.followLocation = !param.followLocation
                setFollowLocation()
            }
        }

        view.findViewById<ImageButton>(R.id.ic_map_orientation)?.apply {
            setOnClickListener {
                mMapView?.mapOrientation = 0f
            }
        }
    }


    override fun onResume() {
        super.onResume()
        context?.let { c ->
            PreferenceManager.getDefaultSharedPreferences(c)
                .registerOnSharedPreferenceChangeListener(this)
            if (!mServiceIsConnected) {
                c.bindService(
                    Intent(c, GuideLocationService::class.java),
                        guideLocationConnection, Context.BIND_AUTO_CREATE)
                mServiceIsConnected = true
            }
            Configuration.getInstance().load(c, PreferenceManager.getDefaultSharedPreferences(c))
            getParamsFromSharedPreferences(PreferenceManager.getDefaultSharedPreferences(c))
        }
        mFilteredCategoryTags.clear()
        mFilteredCategoryTags.addAll(context.getFilteredCategoryTagsSet)

        mMapView?.onResume()

        setFollowLocation()

        if (isResumed) {
            setupTileProvider()
            warningOnMapInOfflineMode()
        }
    }


    override fun onPause() {
        super.onPause()
        context?.let { c ->
            PreferenceManager.getDefaultSharedPreferences(c)
                .unregisterOnSharedPreferenceChangeListener(this)
            if (mServiceIsConnected) {
                c.unbindService(guideLocationConnection)
                mServiceIsConnected = false
            }
        }

        mMapView?.onPause()
        val tileSource = mMapView?.tileProvider?.tileSource
        if (tileSource is MapsForgeTileSource) {
            tileSource.dispose()
        }
        mMapTileProvider?.detach()
        mMapTileProvider = null
    }


    internal fun setObject(o: GuideDbObject?, ro: GuidePoi? = null, moveTo: Boolean = false) {
        when (o) {
            is GuideBook -> {
                addPoi(o.poi.orEmpty(), emptySet(), false)
                o.center?.let { position ->
                    if (position.isValid) {
                        param.centerPoint = GeoPoint(position.latitude, position.longitude)
                        if (moveTo) mMapView?.controller?.setCenter(param.centerPoint)
                    }
                }
            }
            is GuidePoi -> {
                o.coordinates?.let { position ->
                    if (position.isValid) {
                        context?.let {c ->
                            val maxNum = PreferenceManager
                                .getDefaultSharedPreferences(c)
                                .getString(getString(R.string.pref_near_poi_max_number_key), String())?.toIntOrNull()
                                ?: resources.getInteger(R.integer.pref_near_poi_max_number_default)

                            val maxDist = PreferenceManager
                                .getDefaultSharedPreferences(c)
                                .getString(getString(R.string.pref_near_poi_max_distance_key), String())?.toIntOrNull()
                                ?: resources.getInteger(R.integer.pref_near_poi_max_distance_default)

                            val nearPoints = c.currentGuidebook.poiWithinRadius(
                                position, maxDist.toDouble(), maxNum, context)
                            addPoi(nearPoints, setOf(o.uuid), true, nrOffset = 0)
                        }

                        param.centerPoint = GeoPoint(position.latitude, position.longitude)
                        if (moveTo) mMapView?.controller?.setCenter(param.centerPoint)
                    } else {
                        addPoi(listOf(o), setOf(o.uuid), true, nrOffset = 0)
                    }

                    val zoom = param.mapZoom + resources.getInteger(R.integer.map_set_object_add_zoom)
                    if (zoom > 0 && moveTo) {
                        mMapView?.controller?.setZoom(zoom)
                    }
                }
            }
            is GuideRoute -> {
                val defaultStyle = Style(null, 0x901010AA.toInt(), 5f, 0x20AA1010)
                val kmlDocument = KmlDocument()
                try {
                    kmlDocument.parseGeoJSON(o.getGeom().toString())
                    val path = kmlDocument.mKmlRoot.buildOverlay(mMapView, defaultStyle, null, kmlDocument) as FolderOverlay
                    mMapView?.overlays?.add(path)
                } catch (e: Exception) {
                    Log.w(tag, e.message.orEmpty())
                }

                val bb = kmlDocument.mKmlRoot.boundingBox

                if (ro is GuidePoi) {
                    addPoi(o.poi.orEmpty(), setOf(ro.uuid), true)
                    ro.coordinates?.let { position ->
                        if (position.isValid) {
                            param.centerPoint = GeoPoint(position.latitude, position.longitude)
                        }
                    }
                } else {
                    addPoi(o.poi.orEmpty(), emptySet(), true)
                    param.centerPoint = bb?.centerWithDateLine
                }

                mMapView?.let {
                    if (moveTo && bb != null) {
                        it.zoomToBoundingBox(bb, false, mapBorderSize)
                        if (it.zoomLevelDouble < 1.0) {
                            it.controller.setZoom(param.mapZoom)
                        }
                    }
                }
            }
        }
    }


    private fun warningOnMapInOfflineMode() {
        if (!param.online && param.mapSource != getString(R.string.mapsforge_offline) && param.showOfflineWarning) {
            (activity as? GuideActivity)
                    ?.isOnlineAllowedSnackbar(R.string.Map_is_working_in_offline_mode_)
                    ?: context?.longToast(R.string.Map_is_working_in_offline_mode_)
            param.showOfflineWarning = false
        }
    }


    private fun setupBingTileProvider(resId: Int): BingMapTileSource? {
        val a = activity
        if (param.bingKey.isBlank()) {
            param.bingKey = a.getConfigFromStorage(R.string.pref_map_bing_api_key_key)
        }

        if (param.bingKey.isBlank() && a is Activity) {
            Snackbar.make(a.findViewById(android.R.id.content),
                    getString(R.string.No_valid_Bing_API_Key), Snackbar.LENGTH_LONG)
                    .setAction(R.string.Set) {
                        val i = Intent(a, SettingsActivity::class.java)
                        i.action = PreferenceFragmentMap::class.java.canonicalName
                        startActivity(i)
                    }
                    .show()
            return null
        }
        BingMapTileSource.setBingKey(param.bingKey)
        val bingTileSource = BingMapTileSource(null)
        when (resId) {
            R.string.bing_aerial -> bingTileSource.style = BingMapTileSource.IMAGERYSET_AERIAL
            R.string.bing_hybrid -> bingTileSource.style = BingMapTileSource.IMAGERYSET_AERIALWITHLABELS
            R.string.bing_road -> bingTileSource.style = BingMapTileSource.IMAGERYSET_ROAD
        }
        return bingTileSource
    }


    private fun setupMapsForgeTileProvider(mapFileName: String): MapsForgeTileSource? {
        MapFile.wayFilterEnabled = true
        MapFile.wayFilterDistance = 20
        MapWorkerPool.DEBUG_TIMING = false

        val (theme, themeName) = try {
            if (param.renderTheme.isNotEmpty()) {
                val split = param.renderTheme.splitDirFilename()
                Pair<XmlRenderTheme, String>(AssetsRenderTheme(context, split.first, split.second), split.second)
            } else {
                Pair<XmlRenderTheme, String>(InternalRenderTheme.DEFAULT, InternalRenderTheme.DEFAULT.name)
            }
        } catch (e: Exception) {
            Log.w(tag, e.message.orEmpty())
            Pair<XmlRenderTheme, String>(InternalRenderTheme.DEFAULT, InternalRenderTheme.DEFAULT.name)
        }

        return when {
            File(mapFileName).isFile -> {
                MapsForgeTileSource.createFromFiles(arrayOf(File(mapFileName)), theme, themeName)
            }
            mapFileName.isBlank() -> {
                context?.longToast(R.string.Offline_map_file_is_not_selected)
                null
            }
            else -> {
                context?.longToast(getString(R.string.Map_file_does_not_exist__).format(mapFileName))
                null
            }
        }
    }


    private fun setupTileProvider() {
        try {
            val tileSource = when (param.mapSource) {
                getString(R.string.mapnik) -> TileSourceFactory.MAPNIK
                getString(R.string.osm_hikebikemap) -> TileSourceFactory.HIKEBIKEMAP
                getString(R.string.opentopo) -> TileSourceFactory.OpenTopo
                getString(R.string.bing_road) -> setupBingTileProvider(R.string.bing_road)
                getString(R.string.bing_aerial) -> setupBingTileProvider(R.string.bing_aerial)
                getString(R.string.bing_hybrid) -> setupBingTileProvider(R.string.bing_hybrid)
                getString(R.string.mapsforge_offline) -> setupMapsForgeTileProvider(param.mapFile)
                else -> null
            } ?: TileSourceFactory.DEFAULT_TILE_SOURCE


            mMapView?.let { m ->
                mMapTileProvider?.detach()
                mMapTileProvider = if (tileSource is MapsForgeTileSource) {
                    if (param.centerPoint == null) {
                        param.centerPoint = tileSource.boundsOsmdroid?.centerWithDateLine
                    }
                    MapsForgeTileProvider(SimpleRegisterReceiver(context), tileSource, null)
                } else {
                    MapTileProviderBasic(context, tileSource)
                }
                m.tileProvider = mMapTileProvider
                m.setUseDataConnection(param.online)
                m.overlayManager.tilesOverlay.setColorFilter(if (param.nightMode) TilesOverlay.INVERT_COLORS else null)
            }
        } catch (e: Exception) {
            context?.toast(getString(R.string.Can_not_setup_map).format(e.message))
        }
    }


    internal fun setBubbleClickListener(listener: OnBubbleClickListener) {
        onBubbleClickListener = listener
    }


    private fun getParamsFromSharedPreferences(p: SharedPreferences?) {
        param.mapZoom = p?.getString(getString(R.string.pref_map_initial_zoom), String())?.toDoubleOrNull() ?: resources.getInteger(R.integer.pref_map_initial_zoom_default).toDouble()
        param.mapSource = (p?.getString(getString(R.string.pref_map_source_key), null)) ?: getString(R.string.pref_map_source_default)
        param.mapFile = p?.getString(getString(R.string.pref_map_custom_file_key), String()).orEmpty()
        param.online = context.isOnlineAllowed
        param.nightMode = p?.getString(getString(R.string.pref_map_night_mode_key), String()) == getString(R.string.YES)
        param.renderTheme = p?.getString(getString(R.string.pref_map_render_theme_key), String()).orEmpty()
        param.bingKey = p?.getString(getString(R.string.pref_map_bing_api_key_key), String()).orEmpty()
    }


    override fun onSharedPreferenceChanged(p: SharedPreferences?, key: String?) {
        when (key) {
            in arrayOf(
                    R.string.pref_guidebook_key,
                    R.string.pref_map_initial_zoom_key,
                    R.string.pref_map_night_mode_key,
                    R.string.pref_map_render_theme_key,
                    R.string.pref_map_bing_api_key_key,
                    R.string.pref_map_source_key,
                    R.string.pref_map_custom_file_key,
                    R.string.pref_online_content_key).map { getString(it) } -> {
                param.showOfflineWarning = true
                getParamsFromSharedPreferences(p)
                setupTileProvider()
                warningOnMapInOfflineMode()
            }
            getString(R.string.pref_filtered_categories_key) -> {
                mFilteredCategoryTags.clear()
                mFilteredCategoryTags.addAll(context.getFilteredCategoryTagsSet)
                addFilteredPoiClusterToMap()
            }
        }
    }


    private fun addPoi(poiList: Collection<GuidePoi>, highlight: Collection<UUID>, printOrder: Boolean, nrOffset: Int = 1) {
        mPoiMarkers.clear()
        mMapView?.let { mapView ->
            poiList.map { Pair(it, it.coordinates) }.forEachIndexed { index, (poi, position) ->
                if (position?.isValid == true) {
                    val m = Marker(mapView)
                    context?.let { c ->
                        m.title = c.getCategoryLabel(poi.category)
                        val makeHighlight = poi.uuid in highlight
                        if (printOrder && !makeHighlight) {
                            m.icon = c.writeOnDrawableResource(R.drawable.marker_blank_yellow, (index + nrOffset).toString())
                        } else {
                            m.icon = c.getCategoryIcon(poi.category, makeHighlight)
                        }
                    }
                    m.snippet = poi.name
                    m.position = GeoPoint(position.latitude, position.longitude)

                    m.image = if (poi.thumbnailIsEmpty()) {
                        ResourcesCompat.getDrawable(resources, R.drawable.default_thumbnail, null)
                    } else {
                        BitmapDrawable(resources, poi.thumbnail)
                    }

                    m.setInfoWindow(PoiInfoWindow(mMapView))
                    m.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
                    m.relatedObject = poi

                    mPoiMarkers.add(m)
                }
            }
        }
        addFilteredPoiClusterToMap()
    }


    private fun addFilteredPoiClusterToMap() {
        mPoiCluster?.items?.clear()

        mPoiMarkers.forEach {
            val poi = it.relatedObject
            if (poi is GuidePoi && !poi.isCategoryFiltered(context, mFilteredCategoryTags)) {
                mPoiCluster?.add(it)
            }
        }
        mPoiCluster?.invalidate()
        mMapView?.invalidate()
    }

    override fun onDetach() {
        mMapView?.onDetach()
        AndroidGraphicFactory.clearResourceMemoryCache()
        super.onDetach()
    }


    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }


    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onWriteExternalStoragePermissionShowRationale(request: PermissionRequest) {
        activity?.let { a ->
            AlertDialog.Builder(a)
                    .setMessage(R.string.MobiGuide_require_storage_access_to_store_map_tiles)
                    .setPositiveButton(R.string.Yes) { _, _ -> request.proceed() }
                    .setNegativeButton(R.string.No) { _, _ -> request.cancel() }
                    .show()
        }
    }


    private inner class PoiInfoWindow constructor(mapView: MapView?)
        : MarkerInfoWindow(R.layout.bonuspack_bubble, mapView) {

        var mSelectedPoi: GuidePoi? = null

        init {
            val btn = mView?.findViewById<Button?>(R.id.bubble_moreinfo)
            btn?.setOnClickListener {
                onBubbleClickListener(context, mSelectedPoi)
                closeAllInfoWindowsOn(mapView)
            }
        }

        override fun onOpen(item: Any?) {
            super.onOpen(item)
            closeAllInfoWindowsOn(mapView)
            mView?.findViewById<Button?>(R.id.bubble_moreinfo)?.visibility = View.VISIBLE
            val marker = item as? Marker
            mSelectedPoi = marker?.relatedObject as? GuidePoi
        }
    }
}
