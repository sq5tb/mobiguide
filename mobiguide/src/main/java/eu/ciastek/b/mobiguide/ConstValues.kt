package eu.ciastek.b.mobiguide

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.preference.PreferenceManager
import com.dbflow5.query.*
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import java.net.URL
import java.util.*
import kotlin.collections.List
import kotlin.collections.emptyList
import kotlin.collections.filter
import kotlin.collections.forEach
import kotlin.collections.mutableListOf
import kotlin.collections.set


/* enums */
internal enum class XmlRecordType {
    GUIDEBOOK, MAP, CATEGORIES
}

internal enum class UpgradeStatus {
    UPDATE, OK, INSTALL, NA
}

internal data class UpdateXmlRecord(
        @SerializedName("type")
        var type: XmlRecordType,
        @SerializedName("name")
        var name: String = String(),
        @SerializedName("url")
        var url: URL? = null,
        @SerializedName("version")
        var version: Int = -1,
        @SerializedName("uuid")
        var uuid: UUID = Const.emptyUUID,
        @SerializedName("status")
        var status: UpgradeStatus = UpgradeStatus.UPDATE,
        @SerializedName("target")
        var target: String = String(),
        @SerializedName("size")
        var size: Long = 0L
)

/* cont string definitions */
internal object XmlCategory {
    const val TAG_CATEGORIES = "categories"
    const val TAG_CATEGORY = "category"
    const val ATTR_ID = "id"
    const val ATTR_TAGS = "tags"
    const val TAG_NAME = "name"
    const val ATTR_LANG = "lang"
    const val TAG_THUMBNAIL = "thumbnail"
    const val TAGS_DELIMITER = ","
    const val EMPTY_KEY = ""
}

internal object XmlUpdate {
    const val TAG_GUIDEBOOK = "guidebook"
    const val TAG_MAP = "map"
    const val ATTR_NAME = "name"
    const val TAG_CATEGORIES = "categories"
    const val ATTR_UUID = "uuid"
    const val ATTR_SIZE = "size"
    const val ATTR_VERSION = "version"
    const val ATTR_URL = "url"
}

internal object XmlGuidebook {
    const val TAG_ROUTE_GEOM = "geometry"
    const val TAG_ROUTE_OBJECTS = "objects"
    const val TAG_ROUTE_OBJECT = "object"
    const val TAG_ROUTE = "route"
    const val TAG_POI = "poi"
    const val TAG_DESCRIPTION = "description"
    const val TAG_THUMBNAIL = "thumbnail"
    const val TAG_GUIDE = "guide"
    const val TAG_ROUTES = "routes"
    const val TAG_ATTRACTIONS = "attractions"
    const val TAG_PAGES = "pages"
    const val TAG_PAGE = "page"

    const val ATTR_LATLONG = "position"
    const val ATTR_LIST_POSITION = "position"
    const val ATTR_UUID = "uuid"
    const val ATTR_VERSION = "version"
    const val ATTR_NAME = "name"
    const val ATTR_CATEGORY = "category"
    const val ATTR_BASEURL = "baseurl"
    const val ATTR_CENTER_POINT = "center"
}

internal object IntentParam {
    const val WITH_POINTS = "mapWithPoints"
}


internal object Const {
    const val URL_DATA_DIR = "data"
    const val METADATA_EXT = ".metadata"
    const val FILE_CONFIG = "mobiguide.conf.json"
    const val MAP_EXT = ".map"
    const val UNKNOWN_MARKER = R.drawable.marker_star
    internal val emptyUUID = UUID(0, 0)
    internal val emptyBitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
}


internal object Singleton {
    private var guidebook: GuideBook? = null
    private var holderCategories: List<GuideCategory>? = null
    internal val updates = mutableListOf<UpdateXmlRecord>()

    internal fun resetCurrentGuidebook() {
        guidebook = null
    }

    internal fun resetCurrentCategories() {
        holderCategories = null
    }


    val Context.currentGuidebook: GuideBook
        get() {
            if (guidebook == null) {
                val prefGuideBook = PreferenceManager
                    .getDefaultSharedPreferences(applicationContext)
                    .getString(getString(R.string.pref_guidebook_key), String())
                    .toUuidOrEmpty()
                guidebook = (select from GuideBook::class where (GuideBook_Table.uuid eq prefGuideBook)).result
            }
            return guidebook ?: GuideBook()
        }


    internal val Context.getCategoriesList: List<GuideCategory>
        get() {
            if (holderCategories == null) {
                val def = (select from GuideCategory::class where (GuideCategory_Table.id eq GuideCategory.UNKNOWN)).hasData

                if (!def) {
                    GuideCategory().apply {
                        id = GuideCategory.UNKNOWN
                        tags.add(GuideCategory.UNKNOWN)
                        names[XmlCategory.EMPTY_KEY] = getString(R.string.cat_default)
                        thumbnail = BitmapFactory.decodeResource(resources, Const.UNKNOWN_MARKER)
                    }.save()
                }

                holderCategories = (select from GuideCategory::class).list
            }
            return holderCategories ?: emptyList()
        }


    internal fun Context?.refreshAvailableOnlineStatus() {
        updates.forEach { update -> update.status = UpgradeStatus.INSTALL }

        (select from GuideBook::class)
        //SQLite.select(GuideBook_Table.uuid, GuideBook_Table.name, GuideBook_Table.version)
                .list
                .forEach { installed ->
                    var exists = false
                    updates.filter { it.type == XmlRecordType.GUIDEBOOK && it.uuid == installed.uuid }
                            .forEach { update ->
                                exists = true
                                update.status = when {
                                    update.version > installed.version -> UpgradeStatus.UPDATE
                                    update.version == installed.version -> UpgradeStatus.OK
                                    else -> UpgradeStatus.NA
                                }
                            }
                    if (!exists) {
                        val u = UpdateXmlRecord(XmlRecordType.GUIDEBOOK)
                        u.uuid = installed.uuid
                        u.name = installed.name
                        u.status = UpgradeStatus.NA
                        u.size = 0L
                        updates.add(u)
                    }
                }

        getAppMapMetadataFiles.forEach { installed ->
            val metadata = if (installed.exists()) {
                try {
                    Gson().fromJson(installed.readText(), UpdateXmlRecord::class.java)
                } catch (e: Exception) {
                    UpdateXmlRecord(XmlRecordType.MAP)
                }
            } else {
                UpdateXmlRecord(XmlRecordType.MAP)
            }

            var exists = false
            updates.filter { it.type == XmlRecordType.MAP && it.uuid == metadata.uuid }.forEach { update ->
                exists = true
                update.status = when {
                    update.version > metadata.version -> UpgradeStatus.UPDATE
                    update.version == metadata.version -> UpgradeStatus.OK
                    else -> UpgradeStatus.NA
                }
            }
            if (!exists) {
                if (metadata.uuid == Const.emptyUUID) {
                    metadata.uuid = installed
                            .nameWithoutExtension
                            .replace(Regex("${Const.MAP_EXT}\$"), "")
                            .toUuidOrEmpty()
                }
                if (metadata.uuid == Const.emptyUUID) {
                    metadata.uuid = UUID.randomUUID()
                }

                if (metadata.name.isEmpty()) {
                    metadata.name = String.format("%s\n[%s]", installed.name.toUpperCase(Locale.ROOT), installed.parent)
                }

                metadata.status = UpgradeStatus.NA
                metadata.target = installed.absolutePath
                updates.add(metadata)
            }
        }
    }
}
