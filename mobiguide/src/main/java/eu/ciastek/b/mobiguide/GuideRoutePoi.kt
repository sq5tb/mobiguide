package eu.ciastek.b.mobiguide

import com.dbflow5.annotation.Column
import com.dbflow5.annotation.ForeignKey
import com.dbflow5.annotation.PrimaryKey
import com.dbflow5.annotation.Table
import com.dbflow5.structure.BaseModel
import java.util.*

@Table(database = GuideDatabase::class)
class GuideRoutePoi(
        @ForeignKey(tableClass = GuideRoute::class, saveForeignKeyModel = false)
        var route: UUID? = null,

        @ForeignKey(tableClass = GuidePoi::class, saveForeignKeyModel = false)
        var poi: UUID? = null,

        @Column
        var position: Int = 0
) : BaseModel() {
    @PrimaryKey(autoincrement = true)
    var id: Long = 0
}
