package eu.ciastek.b.mobiguide

import com.dbflow5.annotation.*
import com.dbflow5.query.result
import com.dbflow5.query.select
import java.util.*


/**
 * Page class used by application

 * @author Bogusław Ciastek
 * @version 1.0
 */

@Table(
        database = GuideDatabase::class,
        inheritedColumns = [
            (InheritedColumn(column = Column(), fieldName = "name")),
            (InheritedColumn(column = Column(), fieldName = "desc")),
            (InheritedColumn(column = Column(), fieldName = "category")),
            (InheritedColumn(column = Column(), fieldName = "thumbnail"))],
        inheritedPrimaryKeys = [
            (InheritedPrimaryKey(column = Column(), primaryKey = PrimaryKey(), fieldName = "uuid"))])
internal class GuidePage : GuideDbObject() {

    @ForeignKey(tableClass = GuideBook::class, deleteForeignKeyModel = true)
    var guidebook: UUID? = null

    override val imageBaseUrl: String
        get() = formatBaseUrl(guidebook)

    override val print: String
        get() =
            String.format("PAGE:\nGuideBook: $guidebook\n${super.print}")

    companion object {
        fun getFromDb(_uuid: UUID?): GuidePage? {
            return (select from GuidePage::class where (GuidePage_Table.uuid eq _uuid)).result
        }
    }
}
