package eu.ciastek.b.mobiguide

import android.app.Activity
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.content.res.AppCompatResources
import androidx.preference.PreferenceManager


internal class SelectDialogWithThumbnails(private val cxt: Activity) : AlertDialog.Builder(cxt) {

    private val listItems = mutableMapOf<String, MapWithThumbnailsAdapter.ListElem>()
    private var lv: ListView? = null

    private fun dialogInit() {
        val view = LayoutInflater.from(cxt).inflate(R.layout.dialog_multiselect_with_thumbnails,
                cxt.findViewById(android.R.id.content), false)
        lv = view.findViewById(R.id.items_list)
        lv?.adapter = MapWithThumbnailsAdapter(cxt, listItems)

        setView(view)
        setNegativeButton(R.string.cancel, null)

        val icChecked = AppCompatResources.getDrawable(cxt, R.drawable.ic_check_box_checked)
        view.findViewById<Button>(R.id.button_all)?.apply {
            setCompoundDrawablesWithIntrinsicBounds(null, null, icChecked, null)
            setOnClickListener { _ ->
                (lv?.adapter as? MapWithThumbnailsAdapter)?.let { adapter ->
                    adapter.iterator().forEach {
                        it.value.checked = true
                    }
                    adapter.notifyDataSetChanged()
                }
            }
        }

        val icUnchecked = AppCompatResources.getDrawable(cxt, R.drawable.ic_check_box_unchecked)
        view.findViewById<Button>(R.id.button_none)?.apply {
            setCompoundDrawablesWithIntrinsicBounds(null, null, icUnchecked, null)
            setOnClickListener { _ ->
                (lv?.adapter as? MapWithThumbnailsAdapter)?.let { adapter ->
                    adapter.iterator().forEach {
                        it.value.checked = false
                    }
                    adapter.notifyDataSetChanged()
                }
            }
        }
    }

    private fun getItems(checked: Boolean): Set<String> {
        val result = mutableSetOf<String>()
        (lv?.adapter as? MapWithThumbnailsAdapter)?.let { adapter ->
            adapter.iterator().forEach {
                if (it.value.checked == checked && it.key.isNotBlank()) {
                    result.add(it.key)
                }
            }
        }
        return result
    }

    companion object {
        internal fun showCategories(
                cxt: Activity,
                categories: Collection<GuideCategory>,
                dismissFun: () -> Unit) {

            SelectDialogWithThumbnails(cxt).apply {
                val mFilteredCategoriesSet = PreferenceManager.getDefaultSharedPreferences(cxt)
                    .getStringSet(cxt.getString(R.string.pref_filtered_categories_key), emptySet())
                        ?.toMutableSet() ?: mutableSetOf()
                listItems.clear()
                categories.forEach {
                    listItems[it.id] = MapWithThumbnailsAdapter.ListElem(
                            it.getLocalName(), BitmapDrawable(cxt.resources, it.thumbnail),
                            it.id !in mFilteredCategoriesSet)
                }
                dialogInit()
                setTitle(R.string.Select_categories)
                setPositiveButton(R.string.Set) { _, _ ->
                    mFilteredCategoriesSet.clear()
                    mFilteredCategoriesSet.addAll(getItems(false))

                    val prefEditor = PreferenceManager.getDefaultSharedPreferences(cxt).edit()
                    prefEditor.putStringSet(cxt.getString(R.string.pref_filtered_categories_key), mFilteredCategoriesSet)
                    prefEditor.apply()
                }
                setOnDismissListener { dismissFun() }
                show()
            }
        }

        internal fun showUpdates(
                cxt: Activity,
                records: Collection<UpdateXmlRecord>,
                titleId: Int,
                positiveTextId: Int,
                positiveFun: (selected: Collection<UpdateXmlRecord>) -> Unit = {}) {

            SelectDialogWithThumbnails(cxt).apply {
                listItems.clear()
                records.forEach { record ->
                    val drawable = when (record.type) {
                        XmlRecordType.MAP -> R.drawable.ic_map
                        XmlRecordType.GUIDEBOOK -> R.drawable.ic_guidebook
                        XmlRecordType.CATEGORIES -> R.drawable.ic_filter_list
                    }
                    val s = if (record.size > 0) " [" + humanReadableByteCount(record.size) + "]" else String()
                    listItems[record.uuid.toString()] = MapWithThumbnailsAdapter.ListElem(
                            record.name + s,
                            AppCompatResources.getDrawable(cxt, drawable),
                            false)
                }
                dialogInit()
                setTitle(titleId)
                setPositiveButton(positiveTextId) { d, _ ->
                    val selected = getItems(true)
                    val u = records.filter { it.uuid.toString() in selected }
                    d.dismiss()
                    positiveFun(u)
                }
                show()
            }
        }
    }
}
