package eu.ciastek.b.mobiguide

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.dbflow5.annotation.Database
import com.dbflow5.config.DBFlowDatabase
import com.dbflow5.converter.TypeConverter
import com.dbflow5.data.Blob
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.net.URL


@Database(version = 1)
abstract class GuideDatabase : DBFlowDatabase()


@com.dbflow5.annotation.TypeConverter(allowedSubtypes = [(JSONObject::class)])
internal class JSONConverter : TypeConverter<String, JSONObject>() {
    private val tag = this.javaClass.name

    override fun getDBValue(model: JSONObject?): String? {
        return model?.toString()
    }

    override fun getModelValue(data: String?): JSONObject {
        return try {
            if (data.isNullOrBlank()) {
                JSONObject()
            } else {
                JSONObject(data)
            }
        } catch (e: JSONException) {
            Log.w(tag, e.message.orEmpty())
            JSONObject()
        }
    }
}


class StringListConverter : TypeConverter<String, MutableList<String>>() {
    private val tag = this.javaClass.name

    override fun getDBValue(model: MutableList<String>?): String {
        return Gson().toJson(model.orEmpty())
    }

    override fun getModelValue(data: String?): MutableList<String> {
        return try {
            if (data.isNullOrBlank()) {
                mutableListOf()
            } else {
                Gson().fromJson(data.orEmpty(), Array<String>::class.java).toMutableList()
            }
        } catch (e: JSONException) {
            Log.w(tag, e.message.orEmpty())
            mutableListOf()
        }
    }
}


internal class MapOfStringsConverter : TypeConverter<String, MutableMap<String, String>>() {
    private val tag = this.javaClass.name

    override fun getDBValue(model: MutableMap<String, String>?): String {
        return Gson().toJson(model.orEmpty())
    }

    override fun getModelValue(data: String?): MutableMap<String, String> {
        return try {
            if (data.isNullOrBlank()) {
                mutableMapOf()
            } else {
                Gson().fromJson(data, object : TypeToken<Map<String, String>>() {}.type)
            }
        } catch (e: Exception) {
            Log.w(tag, e.message.orEmpty())
            mutableMapOf()
        }
    }
}


@com.dbflow5.annotation.TypeConverter(allowedSubtypes = [(Bitmap::class)])
internal class BitmapConverter : TypeConverter<Blob, Bitmap>() {
    private val tag = this.javaClass.name

    override fun getDBValue(model: Bitmap?): Blob? {
        val stream = ByteArrayOutputStream()
        model?.compress(Bitmap.CompressFormat.PNG, 0, stream)
        return Blob(stream.toByteArray())
    }

    override fun getModelValue(data: Blob?): Bitmap? {
        return try {
            if (data == null) {
                null
            } else {
                BitmapFactory.decodeByteArray(data.blob, 0, data.blob?.size ?: 0)
            }
        } catch (e: JSONException) {
            Log.w(tag, e.message.orEmpty())
            null
        }
    }
}


@com.dbflow5.annotation.TypeConverter(allowedSubtypes = [(URL::class)])
internal class URLConverter : TypeConverter<String, URL>() {

    override fun getDBValue(model: URL?): String? {
        return model?.toExternalForm()
    }

    override fun getModelValue(data: String?): URL? {
        return data.toUrlOrNull()
    }
}
