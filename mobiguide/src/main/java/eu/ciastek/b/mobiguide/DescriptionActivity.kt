package eu.ciastek.b.mobiguide

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import java.util.*


internal class DescriptionActivity : GuideActivity() {
    private enum class DescType {
        POI, ROUTE, PAGE, OTHER
    }

    private val tabDesc = 0
    private val tabList = 1
    private val tabMap = 2

    private var mObject: GuideDbObject? = null
    private var mRouteObject: GuidePoi? = null
    private var mType = DescType.OTHER
    private var mViewPager: NonSwipeableViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentViewWithToolbar(R.layout.activity_description, true)

        setupIntent(intent)

        mViewPager = findViewById(R.id.viewPager)
        mViewPager?.apply {
            setLockedPages(arrayOf(tabMap))
            adapter = DescriptionFragmentAdapter(supportFragmentManager)

            addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

                override fun onPageSelected(position: Int) {
                    val fragment = (adapter as? DescriptionFragmentAdapter)?.getFragment(position)
                    when (position) {
                        tabDesc -> (fragment as? DescriptionFragment)?.setObject(mObject, mRouteObject)
                        tabList -> (fragment as? PoiListFragment)?.setObject(mObject)
                        tabMap -> (fragment as? MapFragment)?.setObject(mObject, mRouteObject, true)
                    }
                    fragment?.onResume()
                }

                override fun onPageScrollStateChanged(state: Int) {}
            })
        }
        findViewById<TabLayout>(R.id.tabLayout)?.apply {
            setupWithViewPager(mViewPager)
            val pages = mViewPager?.adapter?.count ?: 0
            visibility = if (pages < 2) View.INVISIBLE else View.VISIBLE
        }
    }


    private fun setupIntent(i: Intent?) {
        i?.extras?.let { bundle ->
            when {
                GuidePage::class.java.canonicalName in bundle.keySet() -> {
                    val uuidPage = bundle[GuidePage::class.java.canonicalName] as? UUID
                    if (mObject?.uuid != uuidPage) {
                        setObject(GuidePage.getFromDb(uuidPage))
                    }
                }
                GuideRoute::class.java.canonicalName in bundle.keySet() -> {
                    val uuidRoute = bundle[GuideRoute::class.java.canonicalName] as? UUID
                    val uuidRoutePoi = bundle[GuidePoi::class.java.canonicalName] as? UUID
                    if (mObject?.uuid != uuidRoute) {
                        setObject(GuideRoute.getFromDb(uuidRoute))
                    }
                    if (mRouteObject?.uuid != uuidRoutePoi) {
                        setRouteObject(GuidePoi.getFromDb(uuidRoutePoi))
                    }
                }
                GuidePoi::class.java.canonicalName in bundle.keySet() -> {
                    val uuidPoi = bundle[GuidePoi::class.java.canonicalName] as? UUID
                    if (mObject?.uuid != uuidPoi) {
                        setObject(GuidePoi.getFromDb(uuidPoi))
                    }
                }
            }
        }
    }

    private fun setObject(o: GuideDbObject?) {
        mObject = o
        mRouteObject = null

        mType = when (mObject) {
            is GuidePoi -> DescType.POI
            is GuideRoute -> DescType.ROUTE
            is GuidePage -> DescType.PAGE
            else -> DescType.OTHER
        }

        title = o?.name
    }


    private fun setRouteObject(routePoi: GuidePoi?) {
        mRouteObject = routePoi
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if (mObject is GuideRoute && mRouteObject != null) {
                    setObject(mObject)
                    if (mViewPager?.currentItem == 0) {
                        val fragment = (mViewPager?.adapter as? DescriptionFragmentAdapter)?.getFragment(0)
                        (fragment as? DescriptionFragment)?.setObject(mObject, mRouteObject)
                        fragment?.onResume()
                    } else {
                        mViewPager?.setCurrentItem(0, true)
                    }
                } else {
                    onBackPressed()
                }
                true
            }
            R.id.action_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menu?.addFilterIcon()
        menu?.addSettings(true)
        return true
    }


    private inner class DescriptionFragmentAdapter internal constructor(private val mFragmentManager: FragmentManager)
        : FragmentPagerAdapter(mFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

        private val mFragmentTags: MutableMap<Int, String> = mutableMapOf()
        private val fragments: Array<String> by lazy {
            when (mType) {
                DescType.POI -> arrayOf(getString(R.string.Description), getString(R.string.near_poi), getString(R.string.Map))
                DescType.ROUTE -> arrayOf(getString(R.string.Description), getString(R.string.route_points), getString(R.string.Map))
                DescType.PAGE -> arrayOf(getString(R.string.Description))
                else -> arrayOf(getString(R.string.Description))
            }
        }


        override fun getItem(position: Int): Fragment {
            return when (position) {
                tabDesc -> {
                    DescriptionFragment().apply {
                        setObject(mObject, mRouteObject)
                    }
                }
                tabList -> {
                    PoiListFragment().apply {
                        setPoiListClickListener { parent, _, pos, _ ->
                            val o = parent?.getItemAtPosition(pos) as GuidePoi
                            when (mType) {
                                DescType.ROUTE -> this@DescriptionActivity.setRouteObject(o)
                                else -> this@DescriptionActivity.setObject(o)
                            }
                            mViewPager?.currentItem = 0
                        }
                    }
                }
                tabMap -> {
                    MapFragment().apply {
                        setBubbleClickListener { _, poi ->
                            when (mType) {
                                DescType.ROUTE -> this@DescriptionActivity.setRouteObject(poi)
                                else -> this@DescriptionActivity.setObject(poi)
                            }
                            mViewPager?.currentItem = 0
                        }
                    }
                }
                else -> {
                    MapFragment()
                }
            }
        }


        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val obj = super.instantiateItem(container, position)
            if (obj is Fragment) {
                mFragmentTags[position] = obj.tag.orEmpty()
            }
            return obj
        }


        internal fun getFragment(position: Int): Fragment? {
            val tag = mFragmentTags[position] ?: return null
            return mFragmentManager.findFragmentByTag(tag)
        }

        override fun getCount(): Int = fragments.size

        override fun getPageTitle(position: Int): CharSequence = fragments[position]
    }
}
