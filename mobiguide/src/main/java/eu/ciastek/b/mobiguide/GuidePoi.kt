package eu.ciastek.b.mobiguide

import android.location.Location
import com.dbflow5.annotation.*
import com.dbflow5.query.result
import com.dbflow5.query.select
import java.util.*

/**
 * Point of Interest class used by application

 * @author Bogusław Ciastek
 * @version 1.0
 */

@Table(
        database = GuideDatabase::class,
        inheritedColumns = [
            (InheritedColumn(column = Column(), fieldName = "name")),
            (InheritedColumn(column = Column(), fieldName = "desc")),
            (InheritedColumn(column = Column(), fieldName = "category")),
            (InheritedColumn(column = Column(), fieldName = "thumbnail"))],
        inheritedPrimaryKeys = [
            (InheritedPrimaryKey(column = Column(), primaryKey = PrimaryKey(), fieldName = "uuid"))])
internal class GuidePoi() : GuideDbObject() {

    @ForeignKey(tableClass = GuideBook::class, deleteForeignKeyModel = true)
    var guidebook: UUID? = null

    @ColumnMap
    var coordinates: LatLong? = null

    @ColumnIgnore
    var distance: Float = 0.0f
        private set

    @ColumnIgnore
    var bearing: Float = 0.0f
        private set


    constructor(lat: Double?, lng: Double?) : this() {
        coordinates = LatLong.create(lat, lng)
    }


    override val print: String
        get() =
            String.format("POI:\nGuideBook: $guidebook\nCoordinates: (${coordinates?.latitude} ${coordinates?.longitude})\n${super.print}")


    override val imageBaseUrl: String
        get() = formatBaseUrl(guidebook)


    fun updateBearingAndDistance(start: Location?) {
        coordinates?.let { end ->
            if (start is Location && end.isValid) {
                val result = FloatArray(2)
                Location.distanceBetween(start.latitude, start.longitude, end.latitude, end.longitude, result)
                distance = result[0]
                bearing = result[1]
            }
        }
    }


    internal class DistanceComparator(private var mLocation: LatLong?) : Comparator<GuidePoi> {
        override fun compare(p1: GuidePoi?, p2: GuidePoi?): Int {
            return when {
                p1 == null && p2 == null -> 0
                p1 == null -> -1
                p2 == null -> 1
                else -> {
                    val dist1 = p1.coordinates?.distanceTo(mLocation) ?: Float.NaN
                    val dist2 = p2.coordinates?.distanceTo(mLocation) ?: Float.NaN
                    when {
                        dist1.isNaN() && dist2.isNaN() -> 0
                        dist1.isNaN() -> -1
                        dist2.isNaN() -> 1
                        else -> dist1.compareTo(dist2)
                    }
                }
            }
        }
    }

    companion object {
        fun getFromDb(_uuid: UUID?): GuidePoi? {
            return (select from GuidePoi::class where (GuidePoi_Table.uuid eq _uuid)).result
        }
    }
}
