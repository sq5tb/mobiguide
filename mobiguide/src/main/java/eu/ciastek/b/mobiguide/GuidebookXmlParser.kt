package eu.ciastek.b.mobiguide

import android.util.Xml
import com.dbflow5.query.delete
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.net.URL
import java.util.*


private val ns: String? = null

@Throws(XmlPullParserException::class, IOException::class)
internal fun InputStream.categoriesXmlParse() {
    this.use { stream ->
        Xml.newPullParser().apply {
            setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            setInput(stream, "UTF-8")
            nextTag()
            readCategories()
        }
    }
}


@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readCategories() {
    // deleting previous categories
    Singleton.resetCurrentCategories()
    delete().from(GuideCategory::class)

    require(XmlPullParser.START_TAG, ns, XmlCategory.TAG_CATEGORIES)
    while (next() != XmlPullParser.END_TAG) {
        if (eventType != XmlPullParser.START_TAG) {
            continue
        }
        when (name) {
            XmlCategory.TAG_CATEGORY -> readCategory()
            else -> skip()
        }
    }
}


@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readCategory() {
    require(XmlPullParser.START_TAG, ns, XmlCategory.TAG_CATEGORY)

    val category = GuideCategory().apply {
        id = getAttributeValue(ns, XmlCategory.ATTR_ID) ?: GuideCategory.UNKNOWN
        tags = getAttributeValue(ns, XmlCategory.ATTR_TAGS).split(XmlCategory.TAGS_DELIMITER.toRegex()).toMutableList()

        if (id == GuideCategory.UNKNOWN && !tags.contains(GuideCategory.UNKNOWN)) {
            tags.add(GuideCategory.UNKNOWN)
        }
    }

    while (next() != XmlPullParser.END_TAG) {
        if (eventType != XmlPullParser.START_TAG) {
            continue
        }
        when (name) {
            XmlCategory.TAG_THUMBNAIL -> category.setThumbnailBase64(readThumbnailBase64())
            XmlCategory.TAG_NAME -> {
                val lang = getAttributeValue(ns, XmlCategory.ATTR_LANG) ?: XmlCategory.EMPTY_KEY
                val n = readText()
                category.names[lang] = n
            }
            else -> skip()
        }
    }
    category.save()
}


@Throws(XmlPullParserException::class, IOException::class)
internal fun InputStream.guidebookXmlParse(update: UpdateXmlRecord? = null) {
    this.use { stream ->
        Xml.newPullParser().apply {
            setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            setInput(stream, "UTF-8")
            nextTag()
            readGuide(update)
        }
    }
}


@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readGuide(update: UpdateXmlRecord? = null) {

    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_GUIDE)
    val uuidFromXml = getAttributeValue(ns, XmlGuidebook.ATTR_UUID).toUuidOrEmpty()
    val listOfPoi = mutableListOf<UUID>()
    val listOfRoutePoi = mutableListOf<GuideRoutePoi>()

    // delete previous guidebook
    GuideBook.deleteSelected(listOf(uuidFromXml.toString()))

    // default url based on update url
    val urlAttr = getAttributeValue(ns, XmlGuidebook.ATTR_BASEURL).toUrlOrNull()
    val urlUpdate = update?.url
    val urlBase = urlAttr ?: if (urlUpdate is URL) {
        val urlParent = File(urlUpdate.file).parentFile?.parentFile
        if (urlParent != null)
            URL(urlUpdate.protocol, urlUpdate.host,
                    File(arrayOf(urlParent, Const.URL_DATA_DIR, uuidFromXml)
                            .joinToString(File.separator)).canonicalPath)
        else
            null
    }
    else
        null

    val guidebook = GuideBook().apply {
        setVersion(getAttributeValue(ns, XmlGuidebook.ATTR_VERSION).toIntOrNull())
        category = getAttributeValue(ns, XmlGuidebook.ATTR_CATEGORY).orEmpty()
        name = getAttributeValue(ns, XmlGuidebook.ATTR_NAME).orEmpty()
        uuid = uuidFromXml
        baseUrl = urlBase
        val position = getAttributeValue(ns, XmlGuidebook.ATTR_CENTER_POINT).parsePosition()
        center = LatLong.create(position.first, position.second)
    }

    while (next() != XmlPullParser.END_TAG) {
        if (eventType != XmlPullParser.START_TAG) {
            continue
        }
        when (name) {
            XmlGuidebook.TAG_THUMBNAIL -> guidebook.setThumbnailBase64(readThumbnailBase64())
            XmlGuidebook.TAG_DESCRIPTION -> guidebook.desc = readDescription()
            XmlGuidebook.TAG_ATTRACTIONS -> listOfPoi.addAll(readAttractions(uuidFromXml))
            XmlGuidebook.TAG_ROUTES -> listOfRoutePoi.addAll(readRoutes(uuidFromXml))
            XmlGuidebook.TAG_PAGES -> readPages(uuidFromXml)
            else -> skip()
        }
    }

    listOfRoutePoi
            .filter { it.poi in listOfPoi }
            .forEach { it.save() }

    guidebook.save()
}


@Throws(XmlPullParserException::class, IOException::class)
private fun XmlPullParser.readAttractions(guidebook: UUID): List<UUID> {
    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_ATTRACTIONS)
    val listOfPoi = mutableListOf<UUID>()
    while (next() != XmlPullParser.END_TAG) {
        if (eventType != XmlPullParser.START_TAG) {
            continue
        }

        when (name) {
            XmlGuidebook.TAG_POI -> readPoi(guidebook)?.let { listOfPoi.add(it) }
            else -> skip()
        }
    }
    return listOfPoi
}


@Throws(XmlPullParserException::class, IOException::class)
private fun XmlPullParser.readPages(guidebook: UUID) {
    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_PAGES)
    while (next() != XmlPullParser.END_TAG) {
        if (eventType != XmlPullParser.START_TAG) {
            continue
        }

        when (name) {
            XmlGuidebook.TAG_PAGE -> readPage(guidebook)
            else -> skip()
        }
    }
}

@Throws(XmlPullParserException::class, IOException::class)
private fun XmlPullParser.readRoutes(guidebook: UUID): List<GuideRoutePoi> {
    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_ROUTES)
    val routesPoi = mutableListOf<GuideRoutePoi>()
    while (next() != XmlPullParser.END_TAG) {
        if (eventType != XmlPullParser.START_TAG) {
            continue
        }

        when (name) {
            XmlGuidebook.TAG_ROUTE -> routesPoi.addAll(readRoute(guidebook))
            else -> skip()
        }
    }
    return routesPoi
}

@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readPage(guidebook: UUID) {
    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_PAGE)

    val page = GuidePage().apply {
        this.guidebook = guidebook
        category = getAttributeValue(ns, XmlGuidebook.ATTR_CATEGORY).orEmpty()
        name = getAttributeValue(ns, XmlGuidebook.ATTR_NAME).orEmpty()
        setUuid(getAttributeValue(ns, XmlGuidebook.ATTR_UUID))
    }

    while (next() != XmlPullParser.END_TAG) {
        if (eventType != XmlPullParser.START_TAG) {
            continue
        }
        when (name) {
            XmlGuidebook.TAG_THUMBNAIL -> page.setThumbnailBase64(readThumbnailBase64())
            XmlGuidebook.TAG_DESCRIPTION -> page.desc = readDescription()
            else -> skip()
        }
    }
    page.save()
}


@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readRoute(guidebook: UUID): List<GuideRoutePoi> {
    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_ROUTE)
    val routePoi = mutableListOf<GuideRoutePoi>()

    val route = GuideRoute().apply {
        this.guidebook = guidebook
        category = getAttributeValue(ns, XmlGuidebook.ATTR_CATEGORY).orEmpty()
        name = getAttributeValue(ns, XmlGuidebook.ATTR_NAME).orEmpty()
        setUuid(getAttributeValue(ns, XmlGuidebook.ATTR_UUID))
    }

    while (next() != XmlPullParser.END_TAG) {
        if (eventType != XmlPullParser.START_TAG) {
            continue
        }
        when (name) {
            XmlGuidebook.TAG_THUMBNAIL -> route.setThumbnailBase64(readThumbnailBase64())
            XmlGuidebook.TAG_DESCRIPTION -> route.desc = readDescription()
            XmlGuidebook.TAG_ROUTE_GEOM -> route.setGeom(readRouteGeom())
            XmlGuidebook.TAG_ROUTE_OBJECTS -> routePoi.addAll(readRouteObjects(route.uuid))
            else -> skip()
        }
    }
    route.save()
    return routePoi
}

@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readRouteObjects(route: UUID): List<GuideRoutePoi> {
    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_ROUTE_OBJECTS)
    val routesPoi = mutableListOf<GuideRoutePoi>()

    while (next() != XmlPullParser.END_TAG) {
        if (eventType != XmlPullParser.START_TAG) {
            continue
        }
        when (name) {
            XmlGuidebook.TAG_ROUTE_OBJECT -> {
                val o = readRouteObject()
                if (o.first != Const.emptyUUID && route != Const.emptyUUID) routesPoi.add(GuideRoutePoi(route, o.first, o.second))
            }
            else -> skip()
        }
    }
    return routesPoi
}


@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readRouteObject(): Pair<UUID, Int> {
    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_ROUTE_OBJECT)

    val routePoi = Pair(
            getAttributeValue(ns, XmlGuidebook.ATTR_UUID).toUuidOrEmpty(),
            getAttributeValue(ns, XmlGuidebook.ATTR_LIST_POSITION).toIntOrNull() ?: 0
    )
    skip()

    require(XmlPullParser.END_TAG, ns, XmlGuidebook.TAG_ROUTE_OBJECT)
    return routePoi
}


@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readPoi(guidebook: UUID): UUID? {
    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_POI)

    val poi = GuidePoi().apply {
        this.guidebook = guidebook
        category = getAttributeValue(ns, XmlGuidebook.ATTR_CATEGORY).orEmpty()
        name = getAttributeValue(ns, XmlGuidebook.ATTR_NAME).orEmpty()
        setUuid(getAttributeValue(ns, XmlGuidebook.ATTR_UUID))

        val position = getAttributeValue(ns, XmlGuidebook.ATTR_LATLONG).parsePosition()
        coordinates = LatLong.create(position.first, position.second)
    }

    while (next() != XmlPullParser.END_TAG) {
        if (eventType != XmlPullParser.START_TAG) {
            continue
        }

        when (name) {
            XmlGuidebook.TAG_THUMBNAIL -> poi.setThumbnailBase64(readThumbnailBase64())
            XmlGuidebook.TAG_DESCRIPTION -> poi.desc = readDescription()
            else -> skip()
        }
    }

    return if (poi.coordinates?.isValid == true) {
        poi.save()
        poi.uuid
    } else {
        null
    }
}

@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readDescription(): String {
    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_DESCRIPTION)
    val description = readText()
    require(XmlPullParser.END_TAG, ns, XmlGuidebook.TAG_DESCRIPTION)
    return description
}

@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readThumbnailBase64(): String {
    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_THUMBNAIL)
    val thumbnail = readText()
    require(XmlPullParser.END_TAG, ns, XmlGuidebook.TAG_THUMBNAIL)
    return thumbnail
}

@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readRouteGeom(): String {
    require(XmlPullParser.START_TAG, ns, XmlGuidebook.TAG_ROUTE_GEOM)
    val geometry = readText()
    require(XmlPullParser.END_TAG, ns, XmlGuidebook.TAG_ROUTE_GEOM)
    return geometry
}

@Throws(IOException::class, XmlPullParserException::class)
private fun XmlPullParser.readText(): String {
    var result = String()
    if (next() == XmlPullParser.TEXT) {
        result = text
        nextTag()
    }
    return result
}

@Throws(XmlPullParserException::class, IOException::class)
private fun XmlPullParser.skip() {
    if (eventType != XmlPullParser.START_TAG) {
        throw IllegalStateException()
    }
    var depth = 1
    while (depth != 0) {
        when (next()) {
            XmlPullParser.END_TAG -> depth--
            XmlPullParser.START_TAG -> depth++
        }
    }
}
