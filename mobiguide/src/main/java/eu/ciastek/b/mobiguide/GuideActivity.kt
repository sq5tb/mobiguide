package eu.ciastek.b.mobiguide

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.PowerManager
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.preference.PreferenceManager
import com.dbflow5.query.flowQueryList
import com.dbflow5.query.select
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import eu.ciastek.b.mobiguide.Singleton.getCategoriesList
import eu.ciastek.b.mobiguide.Singleton.refreshAvailableOnlineStatus
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import permissions.dispatcher.*
import splitties.toast.longToast
import splitties.toast.toast
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import java.util.zip.GZIPInputStream


@RuntimePermissions
abstract class GuideActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {
    private val tag = this.javaClass.name

    private var mMenu: Menu? = null
    private var progressBar: ProgressBar? = null
    private var progressBarCircle: ProgressBar? = null
    private var progressInfo: TextView? = null

    override fun onResume() {
        super.onResume()
        PreferenceManager.getDefaultSharedPreferences(applicationContext)
            .registerOnSharedPreferenceChangeListener(this)
        setScreenOn()
    }


    override fun onPause() {
        super.onPause()
        PreferenceManager.getDefaultSharedPreferences(applicationContext)
            .unregisterOnSharedPreferenceChangeListener(this)
    }


    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        when (key) {
            getString(R.string.pref_keep_screen_on) -> setScreenOn()
            getString(R.string.pref_guidebook_key) -> Singleton.resetCurrentGuidebook()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        mMenu = menu
        return true
    }


    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }


    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun startLocationService() {
        startService(Intent(this, GuideLocationService::class.java))
        Log.i(tag, "Starting GuideLocationService...")
    }


    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun onAccessLocationShowRationale(request: PermissionRequest) {
        AlertDialog.Builder(this)
                .setMessage(R.string.MobiGuide_require_location_to_show_user_location)
                .setPositiveButton(R.string.Yes) { _, _ -> request.proceed() }
                .setNegativeButton(R.string.No) { _, _ -> request.cancel() }
                .show()
    }


    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun onAccessLocationPermissionDenied() {
        startService(Intent(this, GuideLocationService::class.java))
        Log.i(tag,"Starting GuideLocationService with limited precision...")
    }


    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun onAccessLocationPermissionNeverAskAgain() {
        startService(Intent(this, GuideLocationService::class.java))
        Log.i(tag, "Starting GuideLocationService with limited precision...")
    }


    protected fun setContentViewWithToolbar(@androidx.annotation.LayoutRes layoutResID: Int, showHomeAsUp: Boolean) {
        setContentView(layoutResID)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(showHomeAsUp)

        progressBar = findViewById(R.id.progressBar)
        progressBarCircle = findViewById(R.id.progressBarCircle)
        progressInfo = findViewById(R.id.textViewProgressInfo)
    }


    private fun MenuItem.setFilterIconTint() {
        val filtered = PreferenceManager.getDefaultSharedPreferences(applicationContext)
            .getStringSet(getString(R.string.pref_filtered_categories_key), emptySet())
        val tintColor = when {
            filtered?.isEmpty() == true -> R.color.actionbarPrimaryText
            else -> R.color.warning
        }
        icon.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                ContextCompat.getColor(this@GuideActivity, tintColor),
                BlendModeCompat.SRC_ATOP
        )
    }


    internal fun Menu.addSettings(asIcon: Boolean = false) {
        this.add(0, R.id.action_settings, 0, getString(R.string.Settings))
        if (asIcon) {
            findItem(R.id.action_settings)?.displayAsAction(R.drawable.ic_settings)
        }
    }


    private fun MenuItem.displayAsAction(resIcon: Int, actionEnum: Int = MenuItem.SHOW_AS_ACTION_IF_ROOM) {
        val drawable = AppCompatResources.getDrawable(this@GuideActivity, resIcon)
        drawable?.let {
            drawable.mutate()
            drawable.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                    ContextCompat.getColor(this@GuideActivity, R.color.actionbarPrimaryText),
                    BlendModeCompat.SRC_ATOP
            )
            icon = drawable
            setShowAsAction(actionEnum)
        }
    }


    internal fun Menu.addFilterIcon() {
        this.add(0, R.id.action_filter, 0, getString(R.string.Filter_categories))
        findItem(R.id.action_filter)?.let { item ->
            item.displayAsAction(R.drawable.ic_filter_list, MenuItem.SHOW_AS_ACTION_ALWAYS)
            item.setOnMenuItemClickListener {
                SelectDialogWithThumbnails.showCategories(
                        this@GuideActivity, getCategoriesList) { item.setFilterIconTint() }
                true
            }
            item.setFilterIconTint()
        }
    }


    private fun setScreenOn() {
        if (PreferenceManager.getDefaultSharedPreferences(applicationContext)
                .getBoolean(getString(R.string.pref_keep_screen_on_key), getString(R.string.pref_keep_screen_on_default) == "true")) {
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        }
    }


    internal fun isOnlineAllowedSnackbar(msgId: Int): Boolean {
        val prefOnline = isOnlineAllowed
        if (!prefOnline) {
            runOnUiThread {
                val s = Snackbar.make(findViewById(android.R.id.content),
                        getString(msgId), Snackbar.LENGTH_LONG)
                if (this !is SettingsActivity) {
                    s.setAction(R.string.Settings) {
                        val i = Intent(this, SettingsActivity::class.java)
                        i.action = PreferenceFragmentGeneral::class.java.canonicalName
                        startActivity(i)
                    }
                }
                s.show()
            }
        }
        return prefOnline
    }


    internal fun hideProgressBar() {
        progressBar?.visibility = View.GONE
        progressBarCircle?.visibility = View.GONE
        progressInfo?.visibility = View.GONE
    }


    internal fun showProgressBarIndeterminate(message: String = "") {
        progressBar?.visibility = View.GONE
        progressBarCircle?.visibility = View.VISIBLE
        if (message.isNotBlank()) {
            progressInfo?.visibility = View.VISIBLE
            progressInfo?.text = message
        } else {
            progressInfo?.visibility = View.GONE
        }
    }


    @Suppress("SameParameterValue")
    private fun setProgressBar(progress: Int, message: String = "") {
        progressBarCircle?.let { pc ->
            if (pc.visibility == View.VISIBLE) pc.visibility = View.GONE
        }

        progressBar?.let { p ->
            if (p.visibility != View.VISIBLE) p.visibility = View.VISIBLE
            p.progress = progress
        }

        progressInfo?.let { i ->
            if (message.isNotBlank()) {
                if (i.visibility != View.VISIBLE) i.visibility = View.VISIBLE
                i.text = message
            } else {
                if (i.visibility == View.VISIBLE) i.visibility = View.GONE
            }
        }
    }


    private fun updateProgressBar(progress: Int) {
        progressBar?.let { p ->
            if (p.visibility != View.VISIBLE) p.visibility = View.VISIBLE
            if (p.isIndeterminate) p.isIndeterminate = false
            p.progress = progress
        }
    }


    internal fun doInstallOrUpdateAsync(updates: Collection<UpdateXmlRecord>, fun0: () -> Unit = {}, ignoreOfflineStatus: Boolean = false) {
        if (updates.isEmpty() || (!ignoreOfflineStatus && !isOnlineAllowedSnackbar(R.string.Please_set_online_mode_to_download_data))) return

        val mapDir = PreferenceManager.getDefaultSharedPreferences(applicationContext)
            .getString(getString(R.string.pref_map_default_dir_key), null)
        if (updates.any { it.type == XmlRecordType.MAP && (mapDir.isNullOrBlank() || !File(mapDir).canWrite()) }) {
            val s = Snackbar.make(findViewById(android.R.id.content), R.string.Default_map_dir_is_not_selected, Snackbar.LENGTH_LONG)
            if (this !is SettingsActivity) {
                s.setAction(R.string.Settings) {
                    val i = Intent(this, SettingsActivity::class.java)
                    i.action = PreferenceFragmentMap::class.java.canonicalName
                    startActivity(i)
                }
            }
            s.show()
            return
        }

        GlobalScope.launch {
            var mWakeLock: PowerManager.WakeLock? = null
            try {
                runOnUiThread {
                    setProgressBar(0, getString(R.string.Starting_download_))
                }

                val pm = getSystemService(Context.POWER_SERVICE) as? PowerManager
                mWakeLock = pm?.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, this::class.java.name)
                mWakeLock?.acquire(10 * 60 * 1000)

                installOrUpdateRecords(updates)

                refreshAvailableOnlineStatus()
                fun0()
                runOnUiThread {
                    toast(R.string.Update_process_finished)
                }
            } catch (e: Exception) {
                runOnUiThread {
                    longToast(getString(R.string.Unexpected_error_during_update_download).format(e.message))
                }
            } finally {
                withContext(Dispatchers.Main) {
                    hideProgressBar()
                }
                mWakeLock?.release()
            }
        }
    }


    @Throws(Exception::class)
    private fun installOrUpdateRecords(updates: Collection<UpdateXmlRecord>) {
        val targetMapDir = PreferenceManager.getDefaultSharedPreferences(applicationContext).getString(getString(R.string.pref_map_default_dir_key), null)

        updates.forEachIndexed { i, update ->
            val connection = update.url?.openConnection() as? HttpURLConnection ?: return

            val targetFile = when (update.type) {
                XmlRecordType.GUIDEBOOK, XmlRecordType.CATEGORIES -> {
                    File.createTempFile(update.type.name, "xml.gz", cacheDir)
                }
                XmlRecordType.MAP -> {
                    if (targetMapDir.isNullOrBlank()) {
                        throw Exception(getString(R.string.Default_map_dir_is_not_selected))
                    }

                    val mapsDir = File(targetMapDir, getString(R.string.maps_dir))
                    if (!mapsDir.exists() && !mapsDir.mkdirs() && !mapsDir.canWrite()) {
                        throw Exception(String.format(getString(R.string.Could_not_create_dir_), parent))
                    }

                    // delete existing maps with same uuid as update
                    getAppMapMetadataFiles.forEach { m ->
                        val uuid = try {
                            Gson().fromJson(m.readText(), UpdateXmlRecord::class.java).uuid
                        } catch (e: Exception) {
                            Log.w(tag, "cannot read stored map uuid from JSON ${m.canonicalPath}: ${e.message}")
                            Const.emptyUUID
                        }
                        if (uuid == update.uuid) {
                            val fileMap = File(m.absolutePath
                                    .replace(Regex("${Const.METADATA_EXT}\$"), ""))

                            if (fileMap.exists()) fileMap.delete()
                            if (m.exists()) m.delete()
                        }
                    }
                    File(mapsDir, update.uuid.toString() + Const.MAP_EXT)
                }
            }

            val msg = StringBuilder(getString(R.string.Downloading_file))
            if (updates.size > 1) {
                msg.append(" " + getString(R.string.number_of_number).format(i + 1, updates.size))
            }
            if (!update.url?.file.isNullOrEmpty()) {
                msg.append(": " + update.url?.file)
            }
            msg.append("...")
            runOnUiThread {
                setProgressBar(0, msg.toString())
            }

            if (connection.responseCode != HttpURLConnection.HTTP_OK) {
                throw Exception(getString(R.string.server_returned_HTTP_code)
                        .format(connection.responseCode, connection.responseMessage))
            } else if (targetFile !is File) {
                throw Exception(getString(R.string.target_is_not_a_file))
            }

            val fileLength = connection.contentLength
            connection.inputStream.use { input ->
                FileOutputStream(targetFile).use { output ->
                    val data = ByteArray(4096)
                    var total = 0L

                    while (true) {
                        val count = input.read(data)
                        if (count <= 0) break

                        total = total.plus(count)
                        runOnUiThread {
                            updateProgressBar(if (fileLength > 0) (total * 100.0 / fileLength).toInt() else 0)
                        }
                        output.write(data, 0, count)
                    }
                }
            }

            when (update.type) {
                XmlRecordType.GUIDEBOOK -> GZIPInputStream(FileInputStream(targetFile)).guidebookXmlParse(update)
                XmlRecordType.CATEGORIES -> GZIPInputStream(FileInputStream(targetFile)).categoriesXmlParse()
                XmlRecordType.MAP -> {
                    val targetFileMetadata = File(targetFile.absolutePath + Const.METADATA_EXT)
                    update.target = targetFileMetadata.absolutePath
                    targetFileMetadata.writeText(Gson().toJson(update))
                }
            }

            if (update.type !in arrayOf(XmlRecordType.MAP) && targetFile.delete()) {
                Log.i("taskGuideDownload", "Temp file=${targetFile.name} successfully deleted!")
            }
        }
    }


    fun cacheAllImages(guidebook: UUID) {
        if (!isOnlineAllowedSnackbar(R.string.Please_set_online_mode_to_fetch_images)) return

        val images = mutableSetOf<String>()
        (select (GuidePoi_Table.guidebook_uuid, GuidePoi_Table.uuid, GuidePoi_Table.desc)
                from GuidePoi::class where (GuidePoi_Table.guidebook_uuid eq guidebook))
                .flowQueryList
                .forEach { poi ->
                    val doc = Jsoup.parse(poi.desc, poi.imageBaseUrl)
                    doc.select("img").forEach {
                        val src = it.attr("abs:src")
                        if (src.isNotBlank()) images.add(src)
                    }
                }

        (select (GuideRoute_Table.guidebook_uuid, GuideRoute_Table.uuid, GuideRoute_Table.desc)
                from GuideRoute::class where (GuideRoute_Table.guidebook_uuid eq guidebook))
                .flowQueryList
                .forEach { route ->
                    val doc = Jsoup.parse(route.desc, route.imageBaseUrl)
                    doc.select("img").forEach {
                        val src = it.attr("abs:src")
                        if (src.isNotBlank()) images.add(src)
                    }
                }

        (select (GuidePage_Table.guidebook_uuid, GuidePage_Table.uuid, GuidePage_Table.desc)
                from GuidePage::class where (GuidePage_Table.guidebook_uuid eq guidebook))
                .flowQueryList
                .forEach { page ->
                    val doc = Jsoup.parse(page.desc, page.imageBaseUrl)
                    doc.select("img").forEach {
                        val src = it.attr("abs:src")
                        if (src.isNotBlank()) images.add(src)
                    }
                }

        (select (GuideBook_Table.uuid, GuideBook_Table.desc) from GuideBook::class
                where(GuideBook_Table.uuid eq guidebook))
                .flowQueryList
                .forEach { gb ->
                    val doc = Jsoup.parse(gb.desc, gb.imageBaseUrl)
                    doc.select("img").forEach {
                        val src = it.attr("abs:src")
                        if (src.isNotBlank()) images.add(src)
                    }
                }

        images.forEach { url ->
            Log.i(tag, "Caching image: $url")
            Picasso.get().load(url).fetch()
        }
    }


    internal fun openAboutDialog() {
        val messageView = LayoutInflater.from(this).inflate(
                R.layout.dialog_about, this.findViewById(android.R.id.content), false)

        val html = """
        <div style="font-size: 80%%">%s</div>
        <hr />
        <div style="font-size: 60%%">%s</div>
        """.trimIndent()
                .format(getString(R.string.ApplicationDescription), getString(R.string.ApplicationCredits))

        messageView.findViewById<WebView>(R.id.about_credits)?.apply {
            settings.builtInZoomControls = true
            settings.defaultTextEncodingName = "utf-8"
            settings.javaScriptEnabled = false

            scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
            isScrollbarFadingEnabled = false

            loadData(html, "text/html; charset=utf-8", "UTF-8")
        }

        val version = try {
            val pInfo = this.packageManager.getPackageInfo(packageName, 0)
            " " + getString(R.string._version_number).format(pInfo.versionName)
        } catch (e : PackageManager.NameNotFoundException) {
            String()
        }

        AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_launcher)
                .setTitle(String.format("%s%s\n%s", getString(R.string.app_name), version, getString(R.string.app_name_description)))
                .setView(messageView)
                .setPositiveButton(R.string.OK, null)
                .show()
    }


    @Synchronized
    internal fun refreshAvailableOnlineContent(ignoreOfflineStatus: Boolean = false, showSnackbar: Boolean = true) {
        if (!ignoreOfflineStatus && !isOnlineAllowedSnackbar(R.string.Please_set_online_mode_to_check_updates)) return
        try {
            runOnUiThread {
                showProgressBarIndeterminate()
            }
            synchronized(Singleton.updates) {
                val paramUrl = PreferenceManager
                    .getDefaultSharedPreferences(applicationContext)
                    .getString(getString(R.string.pref_update_xml_url_key), getString(R.string.pref_update_xml_url_default))

                Singleton.updates.clear()
                URL(paramUrl).openStream().use {
                    val input = GZIPInputStream(it)

                    val factory = XmlPullParserFactory.newInstance()
                    factory.isNamespaceAware = true

                    val parser = factory.newPullParser()
                    parser.setInput(input, "UTF-8")

                    var eventType = parser.eventType
                    while (eventType != XmlPullParser.END_DOCUMENT) {
                        if (eventType == XmlPullParser.START_TAG) {
                            val u = when (parser.name) {
                                XmlUpdate.TAG_GUIDEBOOK -> UpdateXmlRecord(XmlRecordType.GUIDEBOOK)
                                XmlUpdate.TAG_MAP -> UpdateXmlRecord(XmlRecordType.MAP)
                                XmlUpdate.TAG_CATEGORIES -> UpdateXmlRecord(XmlRecordType.CATEGORIES)
                                else -> null
                            }
                            if (u is UpdateXmlRecord) {
                                u.name = parser.getAttributeValue(null, XmlUpdate.ATTR_NAME).orEmpty()
                                u.version = parser.getAttributeValue(null, XmlUpdate.ATTR_VERSION)?.toIntOrNull() ?: -1
                                u.uuid = parser.getAttributeValue(null, XmlUpdate.ATTR_UUID).toUuidOrEmpty()
                                u.size = parser.getAttributeValue(null, XmlUpdate.ATTR_SIZE).toLongOrNull() ?: 0L
                                u.url = parser.getAttributeValue(null, XmlUpdate.ATTR_URL).toUrlOrNull()
                                if (u.url is URL) {
                                    Singleton.updates.add(u)
                                }
                            }
                        }
                        eventType = parser.next()
                    }
                }
                refreshAvailableOnlineStatus()

                val updates = Singleton.updates.filter { it.status == UpgradeStatus.UPDATE }
                if (showSnackbar && updates.isNotEmpty()) {
                    runOnUiThread {
                        val s = Snackbar.make(findViewById(android.R.id.content),
                                getString(R.string.Updates_are_available), Snackbar.LENGTH_LONG)
                        if (this !is SettingsActivity) { // Settings Activity has got appropriate option
                            s.setAction(R.string.Update) { _ ->
                                SelectDialogWithThumbnails.showUpdates(this, updates,
                                        R.string.Select_elements_to_update_,
                                        R.string.Update
                                ) { selected ->
                                    if (selected.isEmpty()) return@showUpdates
                                    val u = selected.toMutableList()
                                    u.addAll(Singleton.updates.filter { it.type == XmlRecordType.CATEGORIES })
                                    doInstallOrUpdateAsync(u)
                                }
                            }
                        }
                        s.show()
                    }
                }
            }
        } catch (e: Exception) {
            runOnUiThread {
                toast(getString(R.string.Unexpected_error_during_update_check).format(e.message))
            }
        } finally {
            runOnUiThread { hideProgressBar() }
        }
    }
}