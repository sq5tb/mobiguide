package eu.ciastek.b.mobiguide

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.text.InputFilter
import android.text.InputType
import androidx.preference.EditTextPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat

class PreferenceFragmentLocationService : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_location_service, rootKey)

        findPreference<EditTextPreference>(getString(R.string.pref_min_distance_location_updates_key))?.let {
            SettingsActivity.bindPreferenceSummaryToValue(it)
            it.setOnBindEditTextListener { editText ->
                editText.inputType = InputType.TYPE_CLASS_NUMBER
                editText.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(6))
            }
        }

        findPreference<EditTextPreference>(getString(R.string.pref_min_time_bw_location_updates_key))?.let{
            SettingsActivity.bindPreferenceSummaryToValue(it)
            it.setOnBindEditTextListener { editText ->
                editText.inputType = InputType.TYPE_CLASS_NUMBER
                editText.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(4))
            }
        }

        findPreference<Preference>(getString(R.string.pref_location_system_settings_key))?.setOnPreferenceClickListener {
            startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            true
        }
    }
}
