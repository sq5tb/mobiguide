package eu.ciastek.b.mobiguide

import com.dbflow5.annotation.*
import com.dbflow5.query.result
import com.dbflow5.query.select
import com.dbflow5.structure.oneToMany
import org.json.JSONObject
import java.util.*

/**
 * Route with POI class used by application

 * @author Bogusław Ciastek
 * @version 1.0
 */

@Table(
        database = GuideDatabase::class,
        inheritedColumns = [
            (InheritedColumn(column = Column(), fieldName = "name")),
            (InheritedColumn(column = Column(), fieldName = "desc")),
            (InheritedColumn(column = Column(), fieldName = "category")),
            (InheritedColumn(column = Column(), fieldName = "thumbnail"))],
        inheritedPrimaryKeys = [
            (InheritedPrimaryKey(column = Column(), primaryKey = PrimaryKey(), fieldName = "uuid"))])
internal class GuideRoute : GuideDbObject() {

    @ForeignKey(tableClass = GuideBook::class, deleteForeignKeyModel = true)
    var guidebook: UUID? = null

    @Column(typeConverter = JSONConverter::class)
    private var geom = JSONObject()

    override val imageBaseUrl: String
        get() = formatBaseUrl(guidebook)

    fun setGeom(geoJSON: String?) {
        geom = try {
            if(geoJSON != null) JSONObject(geoJSON) else JSONObject()
        } catch (e: Exception) {
            e.printStackTrace()
            JSONObject()
        }
    }

    fun setGeom(geoJSON: JSONObject?) {
        geom = geoJSON ?: JSONObject()
    }

    fun getGeom(): JSONObject = geom


    var poi by oneToMany {
        select from GuidePoi::class innerJoin GuideRoutePoi::class on (GuideRoutePoi_Table.poi_uuid eq GuidePoi_Table.uuid) where (GuideRoutePoi_Table.route_uuid eq uuid)
    }

    override val print: String
        get() =
            String.format("ROUTE:\nGuideBook: $guidebook\nGeom: $geom\n${super.print}")


    companion object {
        fun getFromDb(_uuid : UUID?) : GuideRoute? {
            return (select from GuideRoute::class where(GuideRoute_Table.uuid eq _uuid)).result
        }
    }
}
