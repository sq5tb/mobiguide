package eu.ciastek.b.mobiguide

import android.Manifest
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import org.osmdroid.util.NetworkLocationIgnorer
import org.osmdroid.views.overlay.mylocation.IMyLocationConsumer
import org.osmdroid.views.overlay.mylocation.IMyLocationProvider
import splitties.toast.toast

internal class GuideLocationService : Service(), LocationListener,
        SharedPreferences.OnSharedPreferenceChangeListener, IMyLocationProvider {

    private val tag = this.javaClass.name
    private val mBinder = GuideLocationBinder()

    private var mLocationManager: LocationManager? = null
    private var mMyLocationConsumer: IMyLocationConsumer? = null
    private val mIgnorer = NetworkLocationIgnorer()
    private var mLocation: Location? = null

    private var minDistanceForLocationUpdate = 0 /* meters */
    private var minTimeBwLocationUpdates = 0 /* milliseconds */
    private var init = false

    private val providerPreferences = hashMapOf(
            LocationManager.NETWORK_PROVIDER to arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            LocationManager.GPS_PROVIDER to arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
            LocationManager.PASSIVE_PROVIDER to emptyArray()
    )


    private fun getParamsFromSharedPreferences(p: SharedPreferences) {
        minDistanceForLocationUpdate = p.getString(getString(R.string.pref_min_distance_location_updates_key), String())
                ?.toIntOrNull() ?: resources.getInteger(R.integer.pref_min_distance_for_location_updates_default)

        minTimeBwLocationUpdates = p.getString(getString(R.string.pref_min_time_bw_location_updates_key), String())
                ?.toIntOrNull() ?: resources.getInteger(R.integer.pref_min_time_bw_location_updates_default)
    }


    private fun initLocationService(): Boolean {
        init = false
        if (mLocationManager == null) {
            mLocationManager = getSystemService(Context.LOCATION_SERVICE) as? LocationManager
        }

        stopLocationService()

        providerPreferences.keys
                .filter { it in mLocationManager?.getProviders(true).orEmpty() }
                .forEach { enabledProvider ->
                    val permissionsGranted = providerPreferences[enabledProvider].orEmpty().none { p ->
                        ContextCompat.checkSelfPermission(this, p) != PackageManager.PERMISSION_GRANTED
                    }
                    if (permissionsGranted) {
                        Log.i(tag, "Requesting location updates from provider $enabledProvider")
                        mLocationManager?.requestLocationUpdates(
                                enabledProvider,
                                minTimeBwLocationUpdates.toLong(),
                                minDistanceForLocationUpdate.toFloat(),
                                this)
                        init = true
                    }
                }

        PreferenceManager.getDefaultSharedPreferences(applicationContext)
            .registerOnSharedPreferenceChangeListener(this)
        return init
    }

    private fun stopLocationService() {
        PreferenceManager.getDefaultSharedPreferences(applicationContext)
            .unregisterOnSharedPreferenceChangeListener(this)
        try {
            mLocationManager?.removeUpdates(this)
        } catch (ignored: SecurityException) {
        }
    }

    //
    // Service
    //

    override fun onCreate() {
        super.onCreate()
        getParamsFromSharedPreferences(PreferenceManager.getDefaultSharedPreferences(applicationContext))
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        initLocationService()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        stopLocationService()
        stopLocationProvider()
        super.onDestroy()
    }


    override fun onBind(intent: Intent?): IBinder = mBinder


    internal inner class GuideLocationBinder : Binder() {
        val service: GuideLocationService
            get() = this@GuideLocationService
    }

    //
    // IMyLocationProvider
    //

    override fun startLocationProvider(myLocationConsumer: IMyLocationConsumer?): Boolean {
        mMyLocationConsumer = myLocationConsumer
        return init
    }


    override fun stopLocationProvider() {
        mMyLocationConsumer = null
    }


    override fun destroy() {
        stopLocationProvider()
    }

    //
    // LocationListener
    //

    override fun onLocationChanged(location: Location?) {
        val betterLocation = isBetterLocation(location, mLocation)
        if (betterLocation) {
            sendBroadcast(Intent(BROADCAST_ACTION).putExtra(KEY_LOCATION, location))
            mLocation = location

            // ignore temporary non-gps fix
            if (!mIgnorer.shouldIgnore(location?.provider, System.currentTimeMillis()))
                mMyLocationConsumer?.onLocationChanged(mLocation, this)
        }
    }


    override fun getLastKnownLocation(): Location? {
        return mLocation
    }


    val getLocation: Location?
        get() {
            var location: Location? = null
            providerPreferences.keys
                    .filter { it in mLocationManager?.getProviders(true) ?: emptyList() }
                    .forEach { provider ->
                        val permissionsGranted = providerPreferences[provider].orEmpty().none { p ->
                            ContextCompat.checkSelfPermission(this, p) != PackageManager.PERMISSION_GRANTED
                        }
                        if (permissionsGranted) {
                            location = mLocationManager?.getLastKnownLocation(provider)
                        }
                    }

            return if (isBetterLocation(location, mLocation)) location else mLocation
        }

    override fun onProviderEnabled(provider: String?) {
        // info(getString(R.string.provider_available).format(provider))
        toast(getString(R.string.provider_available).format(provider))
    }

    override fun onProviderDisabled(provider: String?) {
        // warn(getString(R.string.provider_disabled).format(provider))
        toast(getString(R.string.provider_disabled).format(provider))
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
//        val idMsg = when (status) {
//            LocationProvider.OUT_OF_SERVICE -> R.string.provider_out_of_service
//            LocationProvider.TEMPORARILY_UNAVAILABLE -> R.string.provider_unavailable
//            LocationProvider.AVAILABLE -> R.string.provider_available
//            else -> 0
//        }
//        if (idMsg > 0) {
//            toast(getString(idMsg).format(provider))
//            //warn(getString(idMsg).format(provider))
//        }
    }


    /** Determines whether one Location reading is better than the current Location fix
     * Original source: https://developer.android.com/guide/topics/location/strategies.html
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    private fun isBetterLocation(location: Location?, currentBestLocation: Location?): Boolean {
        if (location == null || currentBestLocation == null) {
            // A new location (if exists) is always better than no location
            return location != null
        }

        // time difference
        val timeDelta = location.time - currentBestLocation.time
        val isSignificantlyNewer = timeDelta > minTimeBwLocationUpdates
        val isSignificantlyOlder = timeDelta < -minTimeBwLocationUpdates
        val isNewer = timeDelta > 0

        when {
            isSignificantlyNewer -> return true
            isSignificantlyOlder -> return false
        }

        // location accuracy
        val accuracyDelta = (location.accuracy - currentBestLocation.accuracy).toInt()
        val isLessAccurate = accuracyDelta > 0
        val isMoreAccurate = accuracyDelta < 0
        val isSignificantlyLessAccurate = accuracyDelta > minDistanceForLocationUpdate * ACCURACY_MULTIPLIER

        // provider
        val isFromSameProvider = location.provider == currentBestLocation.provider

        // determine location quality using a combination of timeliness and accuracy
        return when {
            isMoreAccurate -> true
            isNewer && !isLessAccurate -> true
            isNewer && !isSignificantlyLessAccurate && isFromSameProvider -> true
            else -> false
        }
    }


    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (sharedPreferences is SharedPreferences && key is String && key in
                arrayOf(R.string.pref_min_distance_location_updates_key, R.string.pref_min_time_bw_location_updates_key).map { getString(it) }) {
            getParamsFromSharedPreferences(sharedPreferences)
            if (mLocationManager != null) {
                initLocationService()
            }
        }
    }


    companion object {
        internal const val BROADCAST_ACTION = "${BuildConfig.APPLICATION_ID}.LocationChanged"
        internal const val KEY_LOCATION = "location"
        private const val ACCURACY_MULTIPLIER = 100
    }
}
