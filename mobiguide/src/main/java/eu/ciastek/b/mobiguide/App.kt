package eu.ciastek.b.mobiguide

import android.app.Application
import androidx.preference.PreferenceManager
import androidx.appcompat.app.AppCompatDelegate
import com.dbflow5.config.DatabaseConfig
import com.dbflow5.config.FlowConfig
import com.dbflow5.config.FlowManager
import com.dbflow5.database.AndroidSQLiteOpenHelper
import com.squareup.picasso.Picasso


@Suppress("unused")
internal class App : Application() {
    override fun onCreate() {
        super.onCreate()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        // load default preference values
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false)
        PreferenceManager.setDefaultValues(this, R.xml.pref_location_service, false)
        PreferenceManager.setDefaultValues(this, R.xml.pref_map, false)

        Picasso.get().setIndicatorsEnabled(true)

        FlowManager.init(FlowConfig.Builder(this)
                .database(DatabaseConfig.builder(GuideDatabase::class, AndroidSQLiteOpenHelper.createHelperCreator(this))
                        .databaseName("MobileGuideDatabase")
                        .build())
                .build())
    }
}
