package eu.ciastek.b.mobiguide

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import com.dbflow5.annotation.Column
import com.dbflow5.structure.BaseModel
import eu.ciastek.b.mobiguide.Singleton.getCategoriesList
import java.net.URL
import java.util.*

/**
 * Common functions for database objects

 * @author Bogusław Ciastek
 * @version 1.0
 */
internal abstract class GuideDbObject : BaseModel() {
    private val tag = this.javaClass.name

    var name = String()
    var desc = String()
    var category = GuideCategory.UNKNOWN

    @Column(typeConverter = BitmapConverter::class)
    var thumbnail: Bitmap = Const.emptyBitmap
    var uuid: UUID = Const.emptyUUID


    fun setUuid(uuid: String?) {
        this.uuid = uuid.toUuidOrEmpty()
    }

    fun isCategoryFiltered(c: Context?, mFilteredTags: Set<String>): Boolean {
        return when {
            mFilteredTags.contains(category) -> true
            mFilteredTags.contains(GuideCategory.UNKNOWN) -> c?.getCategoriesList?.any{ category in it.tags } != true
            else -> false
        }
    }

    fun setThumbnailBase64(base64thumbnail: String) {
        thumbnail = try {
            if (base64thumbnail.isEmpty()) {
                Const.emptyBitmap
            } else {
                val b = Base64.decode(base64thumbnail, Base64.DEFAULT)
                BitmapFactory.decodeByteArray(b, 0, b.size)
            }
        } catch (e: Exception) {
            Log.w(tag, "Can't decode BASE64 thumbnail (uuid: $uuid) - ${e.message}")
            Const.emptyBitmap
        }
    }


    fun thumbnailIsEmpty(): Boolean {
        return thumbnail.sameAs(Const.emptyBitmap)
    }


    abstract val imageBaseUrl: String


    protected fun formatBaseUrl(uuidGuidebook: UUID?): String {
        val base = GuideBook.getGuidebookBaseUrl(uuidGuidebook)
        return if (base is URL && uuidGuidebook is UUID) {
            base.toExternalForm() + "/" + uuid.toString() + "/"
        } else String()
    }

    open val print: String
        get() =
            String.format("Name: $name\nDesc: $desc\nCategory: $category\nThumbnail: $thumbnail\nUUID: $uuid\n")
}
