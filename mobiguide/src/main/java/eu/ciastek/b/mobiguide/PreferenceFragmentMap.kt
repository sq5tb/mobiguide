package eu.ciastek.b.mobiguide

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import androidx.appcompat.app.AlertDialog
import androidx.preference.*
import com.google.gson.Gson
import eu.ciastek.b.mobiguide.Singleton.refreshAvailableOnlineStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnShowRationale
import permissions.dispatcher.PermissionRequest
import permissions.dispatcher.RuntimePermissions
import splitties.toast.toast
import java.io.File
import java.util.*


@RuntimePermissions
class PreferenceFragmentMap : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        initWriteStorageWithPermissionCheck()

        setPreferencesFromResource(R.xml.pref_map, rootKey)
        context.refreshAvailableOnlineStatus()
        enableMapManaging(context.isOnlineAllowed)


        // prefOnline - disable/enable map managing option
        findPreference<SwitchPreferenceCompat>(getString(R.string.pref_online_content_key))?.setOnPreferenceChangeListener { _, newValue ->
            enableMapManaging(newValue is Boolean && newValue)
            true
        }

        // Offline maps administration
        findPreference<ListPreference>(getString(R.string.pref_map_default_dir_key))?.let { prefMapsDir ->
            setListPreferenceMapsDir(prefMapsDir)
            prefMapsDir.setOnPreferenceClickListener {
                setListPreferenceMapsDir(prefMapsDir)
                false
            }
        }

        findPreference<ListPreference>(getString(R.string.pref_map_custom_file_key))?.let { prefCustomMapFile ->
            setListPreferenceCustomMapFile(prefCustomMapFile)
            prefCustomMapFile.setOnPreferenceClickListener {
                setListPreferenceCustomMapFile(prefCustomMapFile)
                false
            }
        }

        findPreference<Preference>(getString(R.string.pref_refresh_updates_key))?.setOnPreferenceClickListener {
            onPrefRefreshUpdatesClick()
        }

        findPreference<Preference>(getString(R.string.pref_map_download_key))?.setOnPreferenceClickListener {
            onPrefMapDownloadClick()
        }

        findPreference<Preference>(getString(R.string.pref_map_update_key))?.setOnPreferenceClickListener {
            onPrefMapUpdateClick()
        }

        findPreference<Preference>(getString(R.string.pref_map_delete_key))?.setOnPreferenceClickListener {
            onPrefMapDeleteClick()
        }

        findPreference<ListPreference>(getString(R.string.pref_map_source_key))?.let{
            SettingsActivity.bindPreferenceSummaryToValue(it)
        }

        findPreference<EditTextPreference>(getString(R.string.pref_map_initial_zoom_key))?.let{
            SettingsActivity.bindPreferenceSummaryToValue(it)
            it.setOnBindEditTextListener { editText ->
                editText.inputType = InputType.TYPE_CLASS_NUMBER
                editText.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(2))
            }
        }

        findPreference<ListPreference>(getString(R.string.pref_map_night_mode_key))?.let{
            SettingsActivity.bindPreferenceSummaryToValue(it)
        }

        findPreference<ListPreference>(getString(R.string.pref_map_render_theme_key))?.let{
            SettingsActivity.bindPreferenceSummaryToValue(it)
        }

        findPreference<ListPreference>(getString(R.string.pref_map_default_dir_key))?.let{
            SettingsActivity.bindPreferenceSummaryToValue(it)
        }

        findPreference<ListPreference>(getString(R.string.pref_map_custom_file_key))?.let {
            SettingsActivity.bindPreferenceSummaryToValue(it)
        }

    }


    @NeedsPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun initWriteStorage() {
    }


    @SuppressLint("NeedOnRequestPermissionsResult")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
       super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }


    @OnShowRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onWriteExternalStoragePermissionShowRationale(request: PermissionRequest) {
        context?.let { c ->
            AlertDialog.Builder(c)
                    .setMessage(R.string.MobiGuide_require_storage_access_to_store_map_tiles)
                    .setPositiveButton(R.string.Yes) { _, _ -> request.proceed() }
                    .setNegativeButton(R.string.No) { _, _ -> request.cancel() }
                    .show()
        }
    }


    private fun onPrefMapDeleteClick(): Boolean {
        val maps = mutableListOf<UpdateXmlRecord>()
        context?.getAppMapMetadataFiles?.forEach { metadata ->
            val m = try {
                Gson().fromJson(metadata.readText(), UpdateXmlRecord::class.java)
            } catch (e: Exception) {
                UpdateXmlRecord(XmlRecordType.MAP)
            }
            m.target = metadata.absolutePath

            if (m.name.isEmpty()) {
                m.name = String.format("%s\n[%s]", metadata.nameWithoutExtension.toUpperCase(Locale.ROOT), metadata.parent)
            }

            if (m.uuid == Const.emptyUUID) {
                m.uuid = UUID.randomUUID()
            }
            maps.add(m)
        }

        (this@PreferenceFragmentMap.activity as? GuideActivity)?.let { a ->
            SelectDialogWithThumbnails.showUpdates(a, maps,
                    R.string.Select_maps_to_delete_,
                    R.string.Delete
            ) { selected ->
                if (selected.isEmpty()) return@showUpdates
                AlertDialog.Builder(a)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.Deleting_maps))
                        .setMessage(String.format(resources.getQuantityString(R.plurals.Are_you_sure_you_want_to_delete_n_maps_, selected.size), selected.size))
                        .setPositiveButton(R.string.Yes) { _, _ ->
                            GlobalScope.launch {
                                var res = 0
                                withContext(Dispatchers.Main) {
                                    a.showProgressBarIndeterminate()
                                }
                                val confMap = PreferenceManager.getDefaultSharedPreferences(context)
                                    .getString(getString(R.string.pref_map_custom_file_key), null)
                                selected.forEach { map ->
                                    val fileMapMetadata = File(map.target)
                                    val fileMap = File(map.target
                                            .replace(Regex("${Const.METADATA_EXT}\$"), ""))

                                    if (fileMap.exists() && fileMap.delete()) res = res.inc()
                                    if (fileMapMetadata.exists()) fileMapMetadata.delete()
                                    if (!confMap.isNullOrEmpty() && map.target == (confMap + Const.METADATA_EXT)) {
                                        val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
                                        editor?.remove(getString(R.string.pref_map_custom_file_key))
                                        editor?.apply()
                                    }
                                }

                                context.refreshAvailableOnlineStatus()

                                withContext(Dispatchers.Main) {
                                    enableMapManaging(a.isOnlineAllowed)
                                    a.hideProgressBar()
                                }
                                toast(String.format(resources.getQuantityString(R.plurals.maps_deleted_successfully, res), res))
                            }
                        }
                        .setNegativeButton(R.string.NO, null)
                        .show()
            }
        }
        return true
    }


    private fun onPrefMapUpdateClick(): Boolean {
        val updates = Singleton.updates.filter { it.type == XmlRecordType.MAP && it.status == UpgradeStatus.UPDATE }

        (this@PreferenceFragmentMap.activity as? SettingsActivity)?.let { a ->
            SelectDialogWithThumbnails.showUpdates(a, updates,
                    R.string.Select_maps_to_update_,
                    R.string.Update
            ) { selected ->
                a.doInstallOrUpdateAsync(selected, {
                    a.runOnUiThread { enableMapManaging(a.isOnlineAllowed) }
                })
            }
        }
        return true
    }


    private fun onPrefMapDownloadClick(): Boolean {
        val updates = Singleton.updates.filter { it.type == XmlRecordType.MAP && it.status == UpgradeStatus.INSTALL }

        (this@PreferenceFragmentMap.activity as? SettingsActivity)?.let { a ->
            SelectDialogWithThumbnails.showUpdates(a, updates,
                    R.string.Select_maps_to_install_,
                    R.string.Install
            ) { selected ->
                a.doInstallOrUpdateAsync(selected, {
                    a.runOnUiThread { enableMapManaging(a.isOnlineAllowed) }
                })
            }
        }
        return true
    }


    private fun onPrefRefreshUpdatesClick(): Boolean {
        (activity as? SettingsActivity)?.let { a ->
            GlobalScope.launch {
                a.refreshAvailableOnlineContent(showSnackbar = false)
                withContext(Dispatchers.Main) {
                    enableMapManaging(a.isOnlineAllowed)
                }
            }
        }
        return true
    }


    private fun enableMapManaging(online: Boolean) {
        context?.let { c ->
            findPreference<Preference>(c.getString(R.string.pref_map_download_key))?.isEnabled = online && Singleton.updates.any { u ->
                u.status == UpgradeStatus.INSTALL && u.type == XmlRecordType.MAP
            }

            findPreference<Preference>(c.getString(R.string.pref_map_update_key))?.isEnabled = online && Singleton.updates.any { u ->
                u.status == UpgradeStatus.UPDATE && u.type == XmlRecordType.MAP
            }

            findPreference<Preference>(c.getString(R.string.pref_map_delete_key))?.isEnabled = Singleton.updates.any { u ->
                u.status != UpgradeStatus.INSTALL && u.type == XmlRecordType.MAP
            }
        }
    }


    private fun setListPreferenceMapsDir(preference: Preference?) {
        if (preference is ListPreference) {
            val entries = context?.getAppDirs(true)?.map { it.toString() }.orEmpty().toTypedArray()
            preference.entries = entries
            preference.entryValues = entries
            preference.setDefaultValue("1")
        }
    }


    private fun setListPreferenceCustomMapFile(preference: Preference?) {
        if (preference is ListPreference) {
            val e = arrayListOf<String>()
            val ev = arrayListOf<String>()

            context.getAppMapMetadataFiles.forEach { m ->
                try {
                    val json = Gson().fromJson(m.readText(), UpdateXmlRecord::class.java)
                    ev.add(m.absolutePath.replace(Regex("${Const.METADATA_EXT}\$"), ""))
                    e.add(json.name)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            preference.entries = e.toTypedArray()
            preference.entryValues = ev.toTypedArray()
        }
    }
}
