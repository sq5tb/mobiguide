package eu.ciastek.b.mobiguide

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import com.dbflow5.annotation.*
import com.dbflow5.structure.BaseModel
import java.util.*

/**
 * Base GuideCategory database object

 * @author Bogusław Ciastek
 * @version 1.0
 */

@Table(database = GuideDatabase::class)
internal class GuideCategory : BaseModel() {
    private val tag = this.javaClass.name

    @PrimaryKey
    var id = String()

    @Column(typeConverter = BitmapConverter::class)
    var thumbnail: Bitmap = Const.emptyBitmap

    @Column(typeConverter = StringListConverter::class)
    var tags = mutableListOf<String>()

    @Column(typeConverter = MapOfStringsConverter::class)
    var names = mutableMapOf<String, String>()

    fun setThumbnailBase64(base64thumbnail: String) {
        thumbnail = try {
            val b = Base64.decode(base64thumbnail, Base64.DEFAULT)
            BitmapFactory.decodeByteArray(b, 0, b.size)
        } catch (e: Exception) {
            Log.w(tag, "Can't decode BASE64 thumbnail (category: $id) - ${e.message}")
            Const.emptyBitmap
        }
    }

    internal fun getLocalName(): String {
        // Locale.getDefault().getLanguage()       ---> en
        // Locale.getDefault().getISO3Language()   ---> eng
        // Locale.getDefault().getCountry()        ---> US
        // Locale.getDefault().getISO3Country()    ---> USA
        // Locale.getDefault().getDisplayCountry() ---> United States
        // Locale.getDefault().getDisplayName()    ---> English (United States)
        // Locale.getDefault().toString()          ---> en_US
        // Locale.getDefault().getDisplayLanguage()---> English

        val lang = Locale.getDefault().language
        return when {
            names.containsKey(lang) -> names.getValue(lang)
            names.containsKey(XmlCategory.EMPTY_KEY) -> names.getValue(XmlCategory.EMPTY_KEY)
            else -> id
        }
    }

    companion object {
        internal const val UNKNOWN = "__UNKNOWN__"
    }
}
