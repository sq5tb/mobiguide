package eu.ciastek.b.mobiguide

import android.content.*
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment


/**
 * Point of Interest class used by application

 * @author Bogusław Ciastek
 * @version 1.0
 */
class DescriptionFragment : Fragment() {
    private var mObject: GuideDbObject? = null
    private var mRoutePoi: GuidePoi? = null

    private var webView: WebView? = null
    private var textViewName: TextView? = null
    private var imageViewThumbnail: ImageView? = null

    private var imageViewBearing: ImageView? = null
    private var imageViewCategory: ImageView? = null
    private var textViewDistance: TextView? = null

    private var lastBearingRotation = 0f

    // Location receiver
    private var mServiceIsConnected = false
    private var mReceiver = LocationReceiver()
    private var mService: GuideLocationService? = null
    private val guideLocationConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as GuideLocationService.GuideLocationBinder
            mService = binder.service
            this@DescriptionFragment.updateOnChangePosition(mService?.lastKnownLocation)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            mService = null
        }
    }

    private val objDescription
        get() = if (mObject is GuideRoute && mRoutePoi is GuidePoi) mRoutePoi else mObject


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val view = inflater.inflate(R.layout.fragment_description, container, false)
        view?.let { _ ->
            webView = view.findViewById(R.id.webViewDescription)
            textViewName = view.findViewById(R.id.textViewName)
            imageViewThumbnail = view.findViewById(R.id.imageViewThumbnail)

            imageViewBearing = view.findViewById(R.id.imageViewBearing)
            imageViewCategory = view.findViewById(R.id.imageViewCategory)
            textViewDistance = view.findViewById(R.id.textViewDistance)

            context?.let {
                webView?.webViewClient = CachedWebViewClient(it)
            }

            webView?.apply {
                settings.builtInZoomControls = true
                settings.defaultTextEncodingName = "utf-8"
                settings.javaScriptEnabled = false

                scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
                isScrollbarFadingEnabled = false
            }
        }
        return view
    }


    internal fun setObject(obj: GuideDbObject? = null, routePoi: GuidePoi? = null) {
        mObject = obj
        mRoutePoi = routePoi
    }

    private fun updateDescription() {
        val o = objDescription
        if (mServiceIsConnected) {
            context?.unregisterReceiver(mReceiver)
            context?.unbindService(guideLocationConnection)
            mServiceIsConnected = false
        }

        if (o == null) {
            textViewName?.text = getString(R.string.unknown)
            imageViewThumbnail?.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.default_thumbnail, null))
            imageViewCategory?.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.marker_star, null))
            imageViewBearing?.visibility = View.INVISIBLE
            textViewDistance?.visibility = View.INVISIBLE
            return
        }

        textViewName?.text = o.name
        imageViewCategory?.setImageDrawable(context?.getCategoryIcon(o.category))
        when {
            o.thumbnailIsEmpty() -> imageViewThumbnail?.setImageDrawable(
                    ResourcesCompat.getDrawable(resources, R.drawable.default_thumbnail, null))
            else -> imageViewThumbnail?.setImageBitmap(o.thumbnail)
        }

        val base = o.imageBaseUrl
        when {
            base.isNotEmpty() -> {
                webView?.loadDataWithBaseURL(base, o.desc, "text/html", "UTF-8", null)
            }
            else -> {
                webView?.loadData(o.desc, "text/html; charset=utf-8", "UTF-8")
            }
        }

        if (o is GuidePoi) {
            imageViewBearing?.visibility = View.VISIBLE
            textViewDistance?.visibility = View.VISIBLE

            if (!mServiceIsConnected) {
                context?.let { c ->
                    c.bindService(Intent(c, GuideLocationService::class.java), guideLocationConnection, Context.BIND_AUTO_CREATE)
                    c.registerReceiver(mReceiver, IntentFilter(GuideLocationService.BROADCAST_ACTION))
                }
                mServiceIsConnected = true
            }
            updateOnChangePosition(mService?.lastKnownLocation)
        } else {
            imageViewBearing?.visibility = View.INVISIBLE
            textViewDistance?.visibility = View.INVISIBLE
        }
    }


    override fun onResume() {
        super.onResume()
        if (isResumed) {
            updateDescription()
        }
    }


    override fun onPause() {
        super.onPause()
        if (mServiceIsConnected) {
            context?.unregisterReceiver(mReceiver)
            context?.unbindService(guideLocationConnection)
            mServiceIsConnected = false
        }
    }


    private fun updateOnChangePosition(loc: Location?) {
        val o = (objDescription as? GuidePoi)?.coordinates

        if (loc is Location && o?.isValid == true) {
            val result = FloatArray(2)
            Location.distanceBetween(loc.latitude, loc.longitude, o.latitude, o.longitude, result)

            val distance = result[0]
            val az = result[1].azimuth()
            val animSet = createAzimuthRotation(lastBearingRotation, az.toFloat())

            textViewDistance?.text = context?.getString(R.string.Distance_Az)?.format(distance.distanceAsString, az)
            imageViewBearing?.startAnimation(animSet)
            lastBearingRotation = az.toFloat()
        }
    }


    private inner class LocationReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent is Intent && intent.action == GuideLocationService.BROADCAST_ACTION) {
                val loc = intent.getParcelableExtra<Location>(GuideLocationService.KEY_LOCATION)
                this@DescriptionFragment.updateOnChangePosition(loc)
            }
        }
    }
}
