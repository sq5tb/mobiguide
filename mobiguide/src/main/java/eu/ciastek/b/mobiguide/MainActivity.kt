package eu.ciastek.b.mobiguide

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.preference.PreferenceManager
import com.dbflow5.query.hasData
import com.dbflow5.query.select
import com.google.android.material.snackbar.Snackbar
import eu.ciastek.b.mobiguide.Singleton.currentGuidebook
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import splitties.toast.toast
import kotlin.system.exitProcess


class MainActivity : GuideActivity() {
    private val tag = this.javaClass.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentViewWithToolbar(R.layout.activity_main, false)

        startLocationServiceWithPermissionCheck()
        val prefCheckUpdatesAtStartup = PreferenceManager
            .getDefaultSharedPreferences(applicationContext)
            .getBoolean(getString(R.string.pref_check_updates_key), getString(R.string.pref_check_updates_default) == "true")

        if (isOnlineAllowed && isNetworkAvailable && prefCheckUpdatesAtStartup) {
            GlobalScope.launch {
                refreshAvailableOnlineContent(showSnackbar = true)
            }
        }
    }


    private fun selectGuidebook() {
        val editor = PreferenceManager.getDefaultSharedPreferences(applicationContext).edit()
        val guidebooks = GuideBook.mapOfGuidebooks

        if (guidebooks.size > 1) {
            runOnUiThread {
                AlertDialog.Builder(this)
                        .setTitle(getString(R.string.select_guidebook_))
                        .setSingleChoiceItems(guidebooks.values.toTypedArray(),
                                guidebooks.keys.indexOf(currentGuidebook.uuid.toString()),
                                null)
                        .setPositiveButton(R.string.OK) { dialog, _ ->
                            dialog.dismiss()
                            val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition
                            editor.putString(getString(R.string.pref_guidebook_key),
                                    guidebooks.keys.elementAtOrNull(selectedPosition))
                            editor.apply()
                        }
                        .show()
            }
        } else if (guidebooks.size == 1 && currentGuidebook.uuid.toString() != guidebooks.keys.first()) {
            editor.putString(getString(R.string.pref_guidebook_key), guidebooks.keys.first())
            editor.apply()
        } else {
            runOnUiThread {
                toast(R.string.No_more_guidebooks_to_select)
            }
        }
    }

    private fun selectOrDownloadGuidebookWizard() {
        val hasGuidebooks = (select from GuideBook::class).hasData

        if (hasGuidebooks) {
            selectGuidebook()
        } else {
            AlertDialog.Builder(this)
                .setTitle(getString(R.string.First_run_wizard))
                .setMessage(getString(R.string.No_data__Do_you_want_to_run_the_wizard_and_get_guidebooks_from_the_Internet_))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.Yes)) { dialog, _ ->
                    dialog.cancel()
                    GlobalScope.launch {
                        refreshAvailableOnlineContent(ignoreOfflineStatus = true, showSnackbar = false)
                        val updates = Singleton.updates.filter { it.type == XmlRecordType.GUIDEBOOK }
                        withContext(Dispatchers.Main) {
                            SelectDialogWithThumbnails.showUpdates(this@MainActivity, updates,
                                R.string.Select_guidebooks_to_install_,
                                R.string.Install
                            ) { selected ->
                                if (selected.isEmpty()) return@showUpdates
                                val u = selected.toMutableList()
                                u.addAll(Singleton.updates.filter { it.type == XmlRecordType.CATEGORIES })
                                doInstallOrUpdateAsync(u, { this@MainActivity.runOnUiThread { selectGuidebook() } }, ignoreOfflineStatus = true)
                            }
                        }
                    }
                }
                .setNegativeButton(getString(R.string.No)) { dialog, _ ->
                    dialog.cancel()
                }.show()
        }
    }

    override fun onResume() {
        super.onResume()
        setIconAndTitle()
    }


    fun onImageButtonSettingsClick(@Suppress("UNUSED_PARAMETER") view: View) {
        startActivity(Intent(this, SettingsActivity::class.java))
    }


    fun onImageButtonMyPositionClick(@Suppress("UNUSED_PARAMETER") view: View) {
        startActivity(Intent(this, MyPositionActivity::class.java))
    }


    fun onImageButtonInfoClick(@Suppress("UNUSED_PARAMETER") view: View) {
        startActivity(Intent(this, InfoActivity::class.java))
    }


    fun onImageButtonMapClick(@Suppress("UNUSED_PARAMETER") view: View) {
        startActivity(Intent(this, MapActivity::class.java))
    }


    fun onImageButtonPlacesClick(@Suppress("UNUSED_PARAMETER") view: View) {
        startActivity(Intent(this, TouristAttractionsActivity::class.java))
    }


    fun onImageButtonRoutesClick(@Suppress("UNUSED_PARAMETER") view: View) {
        startActivity(Intent(this, RoutesActivity::class.java))
    }


    fun onTextGuidebookNameClick(@Suppress("UNUSED_PARAMETER") view: View) {
        selectOrDownloadGuidebookWizard()
    }


    fun onFloatingActionButtonMainClick(@Suppress("UNUSED_PARAMETER") view: View) {
        selectOrDownloadGuidebookWizard()
    }


    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        super.onSharedPreferenceChanged(sharedPreferences, key)
        when (key) {
            getString(R.string.pref_guidebook_key) -> {
                setIconAndTitle()
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menu?.add(0, R.id.action_change_guidebook, 0, getString(R.string.select_guidebook_))
        menu?.add(0, R.id.action_check_updates, 0, getString(R.string.Check_updates))
        menu?.add(0, R.id.action_restart_location_service, 0, getString(R.string.Location_service_restart))
        menu?.add(0, R.id.action_about_dialog, 0, getString(R.string.pref_header_about))
        menu?.add(0, R.id.action_exit, 0, getString(R.string.Close))
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_check_updates -> {
                GlobalScope.launch {
                    refreshAvailableOnlineContent(showSnackbar = true)
                    if (isOnlineAllowed && Singleton.updates.none { it.status == UpgradeStatus.UPDATE }) {
                        Snackbar
                                .make(findViewById(android.R.id.content), R.string.The_program_data_is_current_, Snackbar.LENGTH_SHORT)
                                .show()
                    }
                }; true
            }
            R.id.action_about_dialog -> {
                openAboutDialog(); true
            }
            R.id.action_restart_location_service -> {
                stopService(Intent(this, GuideLocationService::class.java))
                startLocationServiceWithPermissionCheck()
                toast(R.string.Location_service_restarted)
                true
            }
            R.id.action_change_guidebook -> {
                selectGuidebook(); true
            }
            R.id.action_exit -> {
                stopService(Intent(this, GuideLocationService::class.java))
                Log.i(tag,"Stopping GuideLocationService...")

                @Suppress("CascadeIf")
                if (Build.VERSION.SDK_INT >= 21) {
                    finishAndRemoveTask()
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    finishAffinity()
                } else {
                    finish()
                }

                exitProcess(0)
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setIconAndTitle() {
        findViewById<TextView>(R.id.textViewGuidebookName)?.apply {
            text = currentGuidebook.name.takeIf { it.isNotBlank() }
                    ?: getString(R.string.select_guidebook_)
        }

        supportActionBar?.apply {
            if (currentGuidebook.thumbnailIsEmpty()) {
                setIcon(R.mipmap.ic_launcher)
            } else {
                try {
                    setIcon(BitmapDrawable(resources, currentGuidebook.thumbnail))
                } catch (e: Exception) {
                    setIcon(R.mipmap.ic_launcher)
                    Log.w(tag, "setIconAndTitle() ${e.message}")
                }
            }
        }
    }
}
