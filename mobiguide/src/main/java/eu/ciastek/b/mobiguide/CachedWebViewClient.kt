package eu.ciastek.b.mobiguide

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.net.URLConnection
import java.util.*


internal class CachedWebViewClient(private val context: Context) : WebViewClient() {
    private val tag = this.javaClass.name
    private val mimePNG = "image/png"
    private val mimeJPG = "image/jpeg"
    private val mimeTXT = "text/plain"
    private val mimeHTML = "text/html"

    @Suppress("OverridingDeprecatedMember")
    override fun shouldOverrideUrlLoading(view: WebView, url: String?): Boolean {
        return if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
            val mime = URLConnection.guessContentTypeFromName(url)
            if (mime == mimeHTML) {
                view.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                true
            } else {
                false
            }
        } else {
            false
        }
    }

    @Suppress("OverridingDeprecatedMember", "DEPRECATION")
    override fun shouldInterceptRequest(view: WebView, url: String): WebResourceResponse? {
        val mParamOnline = context.isOnlineAllowed

        val mime = URLConnection.guessContentTypeFromName(url)
        if (mime == null || !mime.startsWith("image")) {
            return getWebResourceResponseFromString()
        }

        if (url.toLowerCase(Locale.ROOT).endsWith("/favicon.ico")) {
            return WebResourceResponse("image/png", null, null)
        }

        return try {
            val img = when (mParamOnline) {
                true -> Picasso.get().load(url)
                        .get()
                else -> Picasso.get().load(url).networkPolicy(NetworkPolicy.OFFLINE)
                        .get()
            }
            Log.i(tag, "Fetching image: $url")
            getWebResourceResponseFromImage(img, mime)
        } catch (e: Exception) {
            Log.w(tag, "Unable to load image $url ${e.message}")
            getWebResourceResponseFromImageResource(R.drawable.ic_image_error, 16, 16)
        }
    }


    @Suppress("unused")
    private fun getWebResourceResponseFromImageResource(resourceId: Int): WebResourceResponse? {
        val img = BitmapFactory.decodeResource(context.resources, resourceId)
        return getWebResourceResponseFromImage(img)
    }


    @Suppress("SameParameterValue")
    private fun getWebResourceResponseFromImageResource(resourceId: Int, targetWidth: Int,
                                                        targetHeight: Int): WebResourceResponse? {
        val img = Picasso.get().load(resourceId).resize(targetWidth, targetHeight).centerInside().get()
        return getWebResourceResponseFromImage(img)
    }


    private fun getWebResourceResponseFromImage(image: Bitmap, mime: String = mimePNG): WebResourceResponse? {
        val stream = ByteArrayOutputStream()
        return when (mime) {
            mimeJPG -> {
                image.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                val byteArrayInputStream = ByteArrayInputStream(stream.toByteArray())
                WebResourceResponse(mimeJPG, "UTF-8", byteArrayInputStream)
            }
            else -> {
                image.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val byteArrayInputStream = ByteArrayInputStream(stream.toByteArray())
                WebResourceResponse(mimePNG, "UTF-8", byteArrayInputStream)
            }
        }
    }


    private fun getWebResourceResponseFromString(str: String = String()): WebResourceResponse? {
        return WebResourceResponse(mimeTXT, "UTF-8", ByteArrayInputStream(str.toByteArray()))
    }
}
