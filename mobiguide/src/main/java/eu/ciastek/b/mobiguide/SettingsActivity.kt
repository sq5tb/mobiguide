package eu.ciastek.b.mobiguide

import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager


class SettingsActivity : GuideActivity(), PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentViewWithToolbar(R.layout.activity_settings, true)

        intent.action?.let { action ->
            if (action.isNotEmpty() && action in setOf(
                    PreferenceFragmentGeneral::class.java.canonicalName.orEmpty(),
                    PreferenceFragmentMap::class.java.canonicalName.orEmpty(),
                    PreferenceFragmentLocationService::class.java.canonicalName.orEmpty()
                )
            ) {
                val fragment = supportFragmentManager
                    .fragmentFactory
                    .instantiate(classLoader, action)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.preferences_fragment, fragment)
                    .commit()
                setToolbarTitle(fragment)
            }
        }
    }

    private fun setToolbarTitle(fragment : Fragment? = null) {
        when(fragment) {
            is PreferenceFragmentGeneral -> supportActionBar?.title = getString(R.string.General_settings)
            is PreferenceFragmentMap -> supportActionBar?.title = getString(R.string.Map_settings)
            is PreferenceFragmentLocationService -> supportActionBar?.title = getString(R.string.Location_service_settings)
            else -> getString(R.string.Settings)
        }
    }

    override fun onPreferenceStartFragment(caller: PreferenceFragmentCompat?, pref: Preference?): Boolean {
        pref?.let {p ->
            val args = p.extras
            val fragment = supportFragmentManager.fragmentFactory.instantiate(
                classLoader,
                p.fragment)
            fragment.arguments = args
            fragment.setTargetFragment(caller, 0)

            supportFragmentManager.beginTransaction()
                    .replace(R.id.preferences_fragment, fragment)
                    .addToBackStack(null)
                    .commit()

            setToolbarTitle(fragment)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed(); true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        supportActionBar?.title = getString(R.string.Settings)
    }


    companion object {
        private val sBindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, value ->
            val stringValue = value.toString()

            preference.summary = if (preference is ListPreference) {
                val index = preference.findIndexOfValue(stringValue)
                preference.entries?.getOrNull(index)
            } else {
                stringValue
            }
            true
        }

        internal fun bindPreferenceSummaryToValue(preference: Preference) {
            preference.onPreferenceChangeListener = sBindPreferenceSummaryToValueListener

            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.context)
                            .getString(preference.key, ""))
        }
    }
}
