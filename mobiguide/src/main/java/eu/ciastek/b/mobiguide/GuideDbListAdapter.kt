package eu.ciastek.b.mobiguide

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.res.ResourcesCompat

internal class GuideDbListAdapter<T : GuideDbObject>(cxt: Context, private val mAllItemsData: List<T>)
    : ArrayAdapter<T>(cxt, R.layout.list_pages_compass, mAllItemsData), Filterable {

    private val mInflater: LayoutInflater = LayoutInflater.from(cxt)
    private val mFilter = GuideListFilter()
    private var mFilteredData = mAllItemsData


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var mConvertView = convertView
        val holder: ViewHolder

        if (mConvertView is View) {
            holder = mConvertView.tag as ViewHolder
        } else {
            mConvertView = mInflater.inflate(R.layout.list_pages_compass, parent, false)

            holder = ViewHolder().apply {
                imageThumbnail = mConvertView?.findViewById(R.id.imageThumbnail)
                textViewTitle = mConvertView?.findViewById(R.id.textViewTitle)
                textViewDescription = mConvertView?.findViewById(R.id.textViewDescription)
                imageBearing = mConvertView?.findViewById(R.id.imageBearing)
            }
            mConvertView?.tag = holder
        }


        val item = getItem(position)
        when {
            item.thumbnailIsEmpty() -> holder.imageThumbnail?.setImageDrawable(
                    ResourcesCompat.getDrawable(context.resources, R.drawable.default_thumbnail, null))
            else -> holder.imageThumbnail?.setImageBitmap(item.thumbnail)
        }
        holder.textViewTitle?.text = item.name

        when (item) {
            is GuidePoi -> {
                val az = item.bearing.azimuth()
                holder.textViewDescription?.text = context
                        .getString(R.string.Distance_Az).format(item.distance.distanceAsString, az)
                val animSet = createAzimuthRotation(holder.rotation, az.toFloat())
                holder.imageBearing?.startAnimation(animSet)
                holder.rotation = az.toFloat()
            }
            else -> {
                holder.imageBearing?.visibility = View.GONE
                holder.textViewDescription?.text = String()
                holder.textViewDescription?.visibility = View.GONE
            }
        }
        return mConvertView ?: super.getView(position, convertView, parent)
    }


    operator fun iterator() = mFilteredData.iterator()

    inline fun forEach(action: (T) -> Unit) {
        mFilteredData.forEach(action)
    }

    override fun getCount(): Int = mFilteredData.size

    override fun getItem(position: Int): T = mFilteredData[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getFilter(): Filter = mFilter

    private inner class GuideListFilter : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            return FilterResults().apply {
                if (constraint?.isNotEmpty() == true) {
                    val v = mAllItemsData.filter {
                        it.name.contains(constraint, true)
                    }
                    values = v
                    count = v.size
                } else {
                    synchronized(this) {
                        values = mAllItemsData
                        count = mAllItemsData.size
                    }
                }
            }
        }


        @Suppress("UNCHECKED_CAST")
        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            if (results is FilterResults) {
                mFilteredData = results.values as List<T>
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder {
        internal var rotation = 0.0f
        internal var imageThumbnail: ImageView? = null
        internal var textViewTitle: TextView? = null
        internal var textViewDescription: TextView? = null
        internal var imageBearing: ImageView? = null
    }
}
