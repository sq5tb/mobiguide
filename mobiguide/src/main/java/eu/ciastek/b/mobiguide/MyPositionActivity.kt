package eu.ciastek.b.mobiguide

import android.content.*
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import java.util.*


/**
 * MyPosition Activity - presents logging messages with actual position and other debug info

 * @author Bogusław Ciastek
 * @version 1.0
 */
internal class MyPositionActivity : GuideActivity() {
    private var mServiceIsConnected = false
    private var mService: GuideLocationService? = null
    private var mReceiver: LocationReceiver = LocationReceiver()

    private var tLat: TextView? = null
    private var tLong: TextView? = null
    private var tProvider: TextView? = null
    private var tLog: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentViewWithToolbar(R.layout.activity_my_position, true)

        tLat = findViewById(R.id.textViewLat)
        tLong = findViewById(R.id.textViewLong)
        tProvider = findViewById(R.id.textViewProvider)
        tLog = findViewById(R.id.textViewLog)

        tLog?.movementMethod = ScrollingMovementMethod()
        tLog?.text = getString(R.string.__history__)

        val icPosition = AppCompatResources.getDrawable(this, R.drawable.ic_gps_fixed)
        findViewById<Button>(R.id.buttonGetPosition)?.apply {
            setCompoundDrawablesWithIntrinsicBounds(icPosition, null, null, null)
        }
        val icMap = AppCompatResources.getDrawable(this, R.drawable.ic_map)
        findViewById<Button>(R.id.buttonShowMap)?.apply {
            setCompoundDrawablesWithIntrinsicBounds(icMap, null, null, null)
        }
    }

    override fun onStart() {
        super.onStart()
        if (!mServiceIsConnected) {
            bindService(Intent(this, GuideLocationService::class.java), guideLocationConnection, Context.BIND_AUTO_CREATE)
            registerReceiver(mReceiver, IntentFilter(GuideLocationService.BROADCAST_ACTION))
            mServiceIsConnected = true
        }
    }


    override fun onStop() {
        if (mServiceIsConnected) {
            unregisterReceiver(mReceiver)
            unbindService(guideLocationConnection)
            mServiceIsConnected = false
        }
        super.onStop()
    }


    private val guideLocationConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            mService = null
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as GuideLocationService.GuideLocationBinder?
            mService = binder?.service
        }
    }


    fun onButtonGetPositionClick(@Suppress("UNUSED_PARAMETER") view: View) {
        val l = mService?.getLocation
        if (l is Location) {
            tLat?.text = String.format(Locale.US, "%.7f", l.latitude)
            tLong?.text = String.format(Locale.US, "%.7f", l.longitude)
            tProvider?.text = l.provider
        } else {
            tLat?.setText(R.string.unknown)
            tLong?.setText(R.string.unknown)
            tProvider?.setText(R.string.no_provider)
        }
    }

    fun onButtonShowMapClick(@Suppress("UNUSED_PARAMETER") view: View) {
        val i = Intent(this, MapActivity::class.java)
        i.putExtra(IntentParam.WITH_POINTS, false)
        startActivity(i)
    }

    private inner class LocationReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent is Intent && intent.action == GuideLocationService.BROADCAST_ACTION) {
                val loc = intent.getParcelableExtra<Location>(GuideLocationService.KEY_LOCATION)
                loc?.let {
                    tLog?.append("\n" + String.format(getString(R.string.position_log_line),
                            it.provider, it.latitude, it.longitude))
                    tLog?.computeScroll()
                }
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menu?.add(0, R.id.action_clear_log, 0, resources.getString(R.string.Clear_log))
        menu?.add(0, R.id.action_location_settings, 0, resources.getString(R.string.Location_settings))
        menu?.add(0, R.id.action_system_location_settings, 0, resources.getString(R.string.System_Location_settings))
        menu?.add(0, R.id.action_restart_location_service, 0, getString(R.string.Location_service_restart))
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_clear_log -> {
                tLog?.setText(R.string.__history__); true
            }
            R.id.action_system_location_settings -> {
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                true
            }
            R.id.action_restart_location_service -> {
                stopService(Intent(this, GuideLocationService::class.java))
                startLocationServiceWithPermissionCheck()
                tLog?.append("\n" + getString(R.string.Location_service_restarted))
                tLog?.computeScroll()
                true
            }
            R.id.action_location_settings -> {
                val i = Intent(this, SettingsActivity::class.java)
                i.action = PreferenceFragmentLocationService::class.java.canonicalName
                startActivity(i)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
