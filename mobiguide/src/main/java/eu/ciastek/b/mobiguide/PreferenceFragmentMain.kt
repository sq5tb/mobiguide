package eu.ciastek.b.mobiguide

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.Preference

@Suppress("unused")
class PreferenceFragmentMain : PreferenceFragmentCompat() {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_main, rootKey)

        findPreference<Preference>(getString(R.string.pref_header_about_key))?.apply {
            summary = try {
                val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                " " + getString(R.string.build_version_).format(pInfo.versionName)
            } catch (e : PackageManager.NameNotFoundException) {
                String()
            }

            setOnPreferenceClickListener {
                (activity as? GuideActivity)?.openAboutDialog()
                true
            }
        }
    }
}
