package eu.ciastek.b.mobiguide

import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.InputType
import androidx.appcompat.app.AlertDialog
import androidx.preference.*
import eu.ciastek.b.mobiguide.Singleton.currentGuidebook
import eu.ciastek.b.mobiguide.Singleton.refreshAvailableOnlineStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import splitties.toast.toast


class PreferenceFragmentGeneral : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_general, rootKey)
        context.refreshAvailableOnlineStatus()
        enableGuidebookManaging(context.isOnlineAllowed)

        // List of guidebooks
        findPreference<ListPreference>(getString(R.string.pref_guidebook_key))?.let { prefGuidebook ->
            setListPreferenceGuidebook(prefGuidebook)
            prefGuidebook.setOnPreferenceClickListener {
                setListPreferenceGuidebook(prefGuidebook)
                false
            }
        }

        // prefOnline - disable/enable guidebook managing option
        findPreference<SwitchPreferenceCompat>(getString(R.string.pref_online_content_key))?.setOnPreferenceChangeListener { _, newValue ->
                    enableGuidebookManaging(newValue is Boolean && newValue)
                    true
                }

        // Guidebooks data administration
        findPreference<Preference>(getString(R.string.pref_refresh_updates_key))?.setOnPreferenceClickListener {
                    onPrefRefreshUpdatesClick()
                }

        findPreference<Preference>(getString(R.string.pref_guidebook_cache_key))?.setOnPreferenceClickListener {
                    onPrefGuidebookCacheClick()
                }

        findPreference<Preference>(getString(R.string.pref_guidebook_download_key))?.setOnPreferenceClickListener {
                    onPrefGuidebookDownloadClick()
                }

        findPreference<Preference>(getString(R.string.pref_guidebook_update_key))?.setOnPreferenceClickListener {
                    onPrefGuidebookUpdateClick()
                }

        findPreference<Preference>(getString(R.string.pref_guidebook_delete_key))?.setOnPreferenceClickListener {
                    onPrefGuidebookDeleteClick()
                }

        findPreference<EditTextPreference>(getString(R.string.pref_update_xml_url_key))?.let{
            SettingsActivity.bindPreferenceSummaryToValue(it)
        }

        findPreference<EditTextPreference>(getString(R.string.pref_near_poi_max_distance_key))?.let{
            SettingsActivity.bindPreferenceSummaryToValue(it)
            it.setOnBindEditTextListener { editText ->
                editText.inputType = InputType.TYPE_CLASS_NUMBER
            }
        }

        findPreference<EditTextPreference>(getString(R.string.pref_near_poi_max_number_key))?.let{
            SettingsActivity.bindPreferenceSummaryToValue(it)
            it.setOnBindEditTextListener { editText ->
                editText.inputType = InputType.TYPE_CLASS_NUMBER
                editText.filters = arrayOf<InputFilter>(LengthFilter(2))
            }
        }

        findPreference<ListPreference>(getString(R.string.pref_guidebook_key))?.let {
            SettingsActivity.bindPreferenceSummaryToValue(it)
        }
    }


    private fun onPrefGuidebookCacheClick(): Boolean {
        (activity as? GuideActivity)?.let { a ->
            if (!a.isOnlineAllowedSnackbar(R.string.Please_set_online_mode_to_check_updates)) return true
            GlobalScope.launch {
                try {
                    withContext(Dispatchers.Main) {
                        a.showProgressBarIndeterminate(getString(R.string.Caching_images_))
                    }
                    a.cacheAllImages(a.currentGuidebook.uuid)
                } catch (e: Exception) {
                    toast(getString(R.string.Unexpected_error_during_update_check).format(e.message))
                } finally {
                    withContext(Dispatchers.Main) {
                        a.hideProgressBar()
                        enableGuidebookManaging(a.isOnlineAllowed)
                    }
                    toast(R.string.Caching_images_process_finished)
                }
            }
        }
        return true
    }


    private fun onPrefGuidebookDeleteClick(): Boolean {
        val updates = Singleton.updates
                .filter { it.type == XmlRecordType.GUIDEBOOK && it.status != UpgradeStatus.INSTALL }

        (this@PreferenceFragmentGeneral.activity as? SettingsActivity)?.let { a ->
            SelectDialogWithThumbnails.showUpdates(a, updates,
                    R.string.Select_guidebooks_to_delete_,
                    R.string.Delete
            ) { selected ->
                if (selected.isEmpty()) return@showUpdates
                AlertDialog.Builder(a)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.Deleting_guidebooks))
                        .setMessage(String.format(resources.getQuantityString(R.plurals.Are_you_sure_you_want_to_delete_n_guidebooks_, selected.size), selected.size))
                        .setPositiveButton(R.string.Yes) { _, _ ->
                            GlobalScope.launch {
                                withContext(Dispatchers.Main) {
                                    a.showProgressBarIndeterminate()
                                }
                                val res = GuideBook.deleteSelected(selected.map { it.uuid.toString() })
                                a.refreshAvailableOnlineStatus()
                                withContext(Dispatchers.Main) {
                                    enableGuidebookManaging(a.isOnlineAllowed)
                                    a.hideProgressBar()
                                }
                                toast(String.format(resources.getQuantityString(R.plurals.guidebooks_deleted_successfully, res), res))
                            }
                        }
                        .setNegativeButton(R.string.No, null)
                        .show()
            }
        }
        return true
    }


    private fun onPrefGuidebookUpdateClick(): Boolean {
        val updates = Singleton.updates
                .filter { it.type == XmlRecordType.GUIDEBOOK && it.status == UpgradeStatus.UPDATE }

        (this@PreferenceFragmentGeneral.activity as? SettingsActivity)?.let { a ->
            SelectDialogWithThumbnails.showUpdates(a, updates,
                    R.string.Select_guidebooks_to_update_,
                    R.string.Update
            ) { selected ->
                if (selected.isNotEmpty()) {
                    val u = selected.toMutableList()
                    u.addAll(Singleton.updates.filter { it.type == XmlRecordType.CATEGORIES })
                    a.doInstallOrUpdateAsync(u, {
                        a.runOnUiThread { enableGuidebookManaging(context.isOnlineAllowed) }
                    })
                }
            }
        }
        return true
    }


    private fun onPrefGuidebookDownloadClick(): Boolean {
        val updates = Singleton.updates
                .filter { it.type == XmlRecordType.GUIDEBOOK && it.status == UpgradeStatus.INSTALL }

        (this@PreferenceFragmentGeneral.activity as? SettingsActivity)?.let { a ->
            SelectDialogWithThumbnails.showUpdates(a, updates,
                    R.string.Select_guidebooks_to_install_,
                    R.string.Install
            ) { selected ->
                if (selected.isNotEmpty()) {
                    val u = selected.toMutableList()
                    u.addAll(Singleton.updates.filter { it.type == XmlRecordType.CATEGORIES })
                    a.doInstallOrUpdateAsync(u, {
                        a.runOnUiThread { enableGuidebookManaging(context.isOnlineAllowed) }
                    })
                }
            }
        }
        return true
    }


    private fun onPrefRefreshUpdatesClick(): Boolean {
        (activity as? SettingsActivity)?.let { a ->
            GlobalScope.launch {
                a.refreshAvailableOnlineContent(showSnackbar = false)
                withContext(Dispatchers.Main) {
                    enableGuidebookManaging(a.isOnlineAllowed)
                }
            }
        }
        return true
    }


    private fun enableGuidebookManaging(online: Boolean) {
        context?.let { c ->
            findPreference<Preference>(c.getString(R.string.pref_guidebook_download_key))?.isEnabled = online && Singleton.updates.any { u ->
                u.status == UpgradeStatus.INSTALL && u.type == XmlRecordType.GUIDEBOOK
            }

            findPreference<Preference>(c.getString(R.string.pref_guidebook_update_key))?.isEnabled = online && Singleton.updates.any { u ->
                u.status == UpgradeStatus.UPDATE && u.type == XmlRecordType.GUIDEBOOK
            }

            findPreference<Preference>(c.getString(R.string.pref_guidebook_delete_key))?.isEnabled = Singleton.updates.any { u ->
                u.status != UpgradeStatus.INSTALL && u.type == XmlRecordType.GUIDEBOOK
            }
        }
    }


    private fun setListPreferenceGuidebook(preference: Preference?) {
        if (preference is ListPreference) {
            val entries = GuideBook.mapOfGuidebooks
            preference.entries = entries.values.toTypedArray()
            preference.entryValues = entries.keys.toTypedArray()
            preference.setDefaultValue("1")
        }
    }
}
