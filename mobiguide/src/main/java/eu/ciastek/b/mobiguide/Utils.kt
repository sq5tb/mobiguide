package eu.ciastek.b.mobiguide

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import android.view.animation.AnimationSet
import android.view.animation.DecelerateInterpolator
import android.view.animation.RotateAnimation
import androidx.appcompat.content.res.AppCompatResources
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import eu.ciastek.b.mobiguide.Singleton.getCategoriesList
import org.osmdroid.tileprovider.util.StorageUtils
import java.io.File
import java.net.URL
import java.util.*
import kotlin.math.ln
import kotlin.math.pow
import kotlin.math.roundToLong


internal fun String?.toUuidOrEmpty(): UUID {
    return try {
        if (isNullOrBlank()) Const.emptyUUID else UUID.fromString(this)
    } catch (e: Exception) {
        Log.w("toUuidOrEmpty", "(UUID $this): ${e.message}")
        Const.emptyUUID
    }
}


internal fun String?.toUrlOrNull(): URL? {
    return try {
        if (isNullOrBlank()) null else URL(this)
    } catch (e: Exception) {
        Log.w("toUrlOrNull", "(URL $this): ${e.message}")
        null
    }
}


fun String?.parsePosition(): Pair<Double?, Double?> {
    val a = orEmpty()
            .split(",".toRegex())
            .dropLastWhile { it.isEmpty() }

    return Pair(
            a.getOrNull(1)?.toDoubleOrNull(),
            a.getOrNull(0)?.toDoubleOrNull()
    )
}


internal fun Context.getCategoryIcon(tag: String, highlight: Boolean = false): Drawable? {
    val display = getSystemService(Context.WINDOW_SERVICE) as? WindowManager
    val cat = getCategoriesList.firstOrNull { tag in it.tags }
    return when {
        highlight -> AppCompatResources.getDrawable(this, R.drawable.ic_place)
        cat == null -> AppCompatResources.getDrawable(this, Const.UNKNOWN_MARKER)
        display is WindowManager -> {
            val metrics = DisplayMetrics()
            display.defaultDisplay.getMetrics(metrics)

            val scaleWidth = metrics.scaledDensity
            val scaleHeight = metrics.scaledDensity

            val matrix = Matrix()
            matrix.postScale(scaleWidth, scaleHeight)

            val resizedBitmap = Bitmap.createBitmap(
                    cat.thumbnail, 0, 0,
                    cat.thumbnail.width, cat.thumbnail.height, matrix, true)

            BitmapDrawable(resources, resizedBitmap)
        }
        else -> BitmapDrawable(resources, cat.thumbnail)
    }
}


internal fun Context.getCategoryLabel(tag: String): String {
    return getCategoriesList
            .firstOrNull { tag in it.tags }?.getLocalName()
            ?: resources.getString(R.string.cat_default)
}


internal val Context?.isOnlineAllowed: Boolean
    get () = if (this is Context) {
        PreferenceManager.getDefaultSharedPreferences(applicationContext).getBoolean(getString(R.string.pref_online_content_key),
                getString(R.string.pref_online_content_default) == "true")
    } else {
        false
    }


internal fun String.splitDirFilename(): Pair<String, String> {
    val split = lastIndexOf('/').plus(1)
    return Pair(substring(0, split), substring(split))
}


internal val File.isNoSymlink: Boolean
    get() = this.canonicalFile == this.absoluteFile


internal val Context?.getFilteredCategoryTagsSet: Set<String>
    get() {
        val result = mutableSetOf<String>()
        if (this is Context) {
            val disabledCategories = PreferenceManager
                .getDefaultSharedPreferences(applicationContext)
                .getStringSet(getString(R.string.pref_filtered_categories_key), emptySet())
            getCategoriesList.asSequence().filter { it.id in disabledCategories.orEmpty() }.map { result.addAll(it.tags) }.toList()
        }
        return result
    }


internal fun Context.getAppDirs(writableOnly: Boolean = true): Set<File> {
    val dirs = mutableSetOf<File>()
    StorageUtils.getStorageList().forEach {
        val dir = File(it.path)
        if (dir.isDirectory && (dir.canWrite() || !writableOnly) && dir.isNoSymlink)
            dirs.add(File(it.path))
    }
    val extDir = getExternalFilesDir(null)
    if (extDir is File && extDir.isDirectory && (extDir.canWrite() || !writableOnly)) {
        dirs.add(extDir)
    }
    return dirs
}

internal fun Context?.getConfigFromStorage(resId: Int): String {
    if (this == null) {
        return String()
    }
    getAppDirs(false).forEach { dir ->
        val jsonFile = File(dir, Const.FILE_CONFIG)
        if (jsonFile.isFile) {
            try {
                val conf: Map<String, String> = Gson().fromJson(jsonFile.readText(), object : TypeToken<Map<String, String>>() {}.type)
                val key = getString(resId)
                if (key in conf.keys) {
                    Log.i("getConfigFromStorage()", "$key = ${conf[key]}")
                    return conf.getValue(key)
                }
            } catch (e: Exception) {
                Log.w("getConfigFromStorage()", e.message.orEmpty())
            }
        }
    }
    return String()
}

internal val Context?.getAppMapMetadataFiles: Set<File>
    get() {
        val maps = mutableSetOf<File>()
        if (this is Context) {
            getAppDirs(false).forEach { f ->
                val files = File(f, "maps").listFiles { pathname ->
                    pathname.name.toLowerCase(Locale.ROOT).endsWith(Const.MAP_EXT + Const.METADATA_EXT)
                }
                files?.indices?.mapTo(maps) { files[it] }
            }
        }
        return maps
    }

internal val Float.distanceAsString: String
    get() = if (this >= 1000) {
        String.format("%.2f km", this / 1000.0)
    } else {
        String.format("%d m", this.roundToLong())
    }


internal fun Float.azimuth(): Long {
    var az = this.roundToLong()
    if (az < 0) az = az.plus(360)
    return az
}


internal fun createAzimuthRotation(fromDegrees: Float, toDegrees: Float): AnimationSet {
    val animSet = AnimationSet(true).apply {
        interpolator = DecelerateInterpolator()
        fillAfter = true
        isFillEnabled = true
    }

    val animRotate = RotateAnimation(fromDegrees, toDegrees,
            RotateAnimation.RELATIVE_TO_SELF, 0.5f,
            RotateAnimation.RELATIVE_TO_SELF, 0.5f)
            .apply {
                duration = 500
                fillAfter = true
            }
    animSet.addAnimation(animRotate)
    return animSet
}


fun Context.writeOnDrawableResource(drawableId: Int, text: String): BitmapDrawable {

    val bitmap = BitmapFactory.decodeResource(resources, drawableId).copy(Bitmap.Config.ARGB_8888, true)

    val paint = Paint().apply {
        style = Paint.Style.FILL_AND_STROKE
        color = Color.BLACK
        textSize = 30f
        typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        textAlign = Paint.Align.CENTER
        isAntiAlias = true
    }

    val canvas = Canvas(bitmap)
    canvas.drawText(text, bitmap.width / 2f, bitmap.height / 2f + 5, paint)

    return BitmapDrawable(resources, bitmap)
}

// original code taken from https://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java

fun humanReadableByteCount(bytes: Long, si: Boolean = false): String {
    val unit = if (si) 1000 else 1024
    if (bytes < unit) return "$bytes B"
    val exp = (ln(bytes.toDouble()) / ln(unit.toDouble())).toInt()
    val pre = (if (si) "kMGTPE" else "KMGTPE")[exp - 1] + if (si) "" else "i"
    return String.format("%.1f %sB", bytes / unit.toDouble().pow(exp), pre)
}

@Suppress("DEPRECATION")
internal val Context.isNetworkAvailable: Boolean
    get() {
        var result = 0 // 0: none; 1: mobile; 2: wifi

        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            connectivityManager?.run {
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)?.run {
                    if (hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = 2
                    } else if (hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = 1
                    }
                }
            }
        } else {
            connectivityManager?.run {
                connectivityManager.activeNetworkInfo?.run {
                    if (type == ConnectivityManager.TYPE_WIFI) {
                        result = 2
                    } else if (type == ConnectivityManager.TYPE_MOBILE) {
                        result = 1
                    }
                }
            }
        }
        return result > 0
    }