package eu.ciastek.b.mobiguide

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

/**
 * ViewPager non swipeable on Map Page

 * @author Bogusław Ciastek
 * @version 1.0
 */
internal class NonSwipeableViewPager : ViewPager {
    private var mLockedPages: Array<Int> = emptyArray()

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)


    internal fun setLockedPages(lockedPages: Array<Int>) {
        mLockedPages = lockedPages
    }


    override fun onInterceptTouchEvent(event: MotionEvent?): Boolean =
            (currentItem !in mLockedPages) && super.onInterceptTouchEvent(event)


    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (currentItem in mLockedPages) {
            return false
        }
        super.onTouchEvent(event)
        performClick()
        return true
    }

    override fun performClick(): Boolean {
        super.performClick()
        return true
    }
}
