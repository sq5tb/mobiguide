package eu.ciastek.b.mobiguide

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.preference.PreferenceManager
import eu.ciastek.b.mobiguide.Singleton.currentGuidebook


internal class TouristAttractionsActivity : GuideActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentViewWithToolbar(R.layout.activity_tourist_attractions, true)

        val listFragment = supportFragmentManager.findFragmentById(R.id.fragmentPoiList) as? PoiListFragment
        listFragment?.setListItems(currentGuidebook.poi.orEmpty())
    }


    override fun onResume() {
        super.onResume()
        val listFragment = supportFragmentManager.findFragmentById(R.id.fragmentPoiList) as? PoiListFragment
        val sort = PreferenceManager.getDefaultSharedPreferences(applicationContext)
            .getBoolean(getString(R.string.pref_distance_sort_key), getString(R.string.pref_distance_sort_default) == "true")
        listFragment?.setDistanceSort(sort)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        super.onSharedPreferenceChanged(sharedPreferences, key)
        when (key) {
            getString(R.string.pref_distance_sort_key) -> {
                val listFragment = supportFragmentManager.findFragmentById(R.id.fragmentPoiList) as? PoiListFragment
                val sort = sharedPreferences?.getBoolean(getString(R.string.pref_distance_sort_key),
                        getString(R.string.pref_distance_sort_default) == "true") ?:
                        getString(R.string.pref_distance_sort_default) == "true"
                listFragment?.setDistanceSort(sort)
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed(); true
            }
            R.id.action_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menu?.addFilterIcon()
        menu?.addSettings(true)
        return true
    }
}
