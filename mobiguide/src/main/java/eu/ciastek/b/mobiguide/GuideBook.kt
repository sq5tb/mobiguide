package eu.ciastek.b.mobiguide

import android.content.Context
import com.dbflow5.annotation.*
import com.dbflow5.query.*
import com.dbflow5.structure.oneToMany
import com.dbflow5.transaction.processTransaction

import java.net.URL
import java.util.*


/**
 * Base GuideBook database object

 * @author Bogusław Ciastek
 * @version 1.0
 */
@Table(
        database = GuideDatabase::class,
        inheritedColumns = [
            (InheritedColumn(column = Column(), fieldName = "name")),
            (InheritedColumn(column = Column(), fieldName = "desc")),
            (InheritedColumn(column = Column(), fieldName = "category")),
            (InheritedColumn(column = Column(), fieldName = "thumbnail"))],
        inheritedPrimaryKeys = [(InheritedPrimaryKey(column = Column(), primaryKey = PrimaryKey(), fieldName = "uuid"))])
internal class GuideBook : GuideDbObject() {
    @Column
    var version: Int = 0
        private set

    @ColumnMap
    var center: LatLong? = null

    @Column(typeConverter = URLConverter::class)
    var baseUrl: URL? = null


    override val imageBaseUrl: String
        get() = formatBaseUrl(uuid)


    @Suppress("unused")
    fun setVersion(version: String) {
        this.version = version.toIntOrNull() ?: 0
    }

    fun setVersion(version: Int?) {
        this.version = version ?: 0
    }

    override val print: String
        get() = String.format("GUIDEBOOK:\nVersion: $version\n${super.print}\nBaseURL: $baseUrl")


    @get:OneToMany(oneToManyMethods = [OneToManyMethod.ALL])
    var poi by oneToMany { select from GuidePoi::class where (GuidePoi_Table.guidebook_uuid eq uuid) }

    @get:OneToMany(oneToManyMethods = [OneToManyMethod.ALL])
    var routes by oneToMany { select from GuideRoute::class where (GuideRoute_Table.guidebook_uuid eq uuid) }

    @get:OneToMany(oneToManyMethods = [OneToManyMethod.ALL])
    var pages by oneToMany { select from GuidePage::class where (GuidePage_Table.guidebook_uuid eq uuid) }


    /**
     * get elements within circle with radius specified by distance parameter

     * @param point Location point.
     * @param radius maximum distance between object and location in meters. If < 0 then returns all.
     * @param count Number of searching nearest objects
     * @return List of objects
     */
    fun poiWithinRadius(point: LatLong?, radius: Double = Double.NaN, count: Int = -1,
                        mContext: Context?): List<GuidePoi> {
        if (point !is LatLong) {
            return emptyList()
        }
        val mFilteredTags = mContext.getFilteredCategoryTagsSet
        val result = (select from GuidePoi::class where (GuidePoi_Table.guidebook_uuid eq uuid))
                .flowQueryList
                .filter {
                    (radius.isNaN() || point.withinRadius(it.coordinates, radius))
                            && (!it.isCategoryFiltered(mContext, mFilteredTags))
                }

        return (if (count > 0) {
            result.asSequence().sortedWith(GuidePoi.DistanceComparator(point)).take(count).toList()
        } else {
            result
        })
    }


    companion object {
        val mapOfGuidebooks: Map<String, String>
            get() {
                val result = mutableMapOf<String, String>()
                (select (GuideBook_Table.uuid, GuideBook_Table.name) from GuideBook::class)
                        .flowQueryList
                        .forEach {
                            result[it.uuid.toString()] = it.name
                        }
                return result
            }


        internal fun getGuidebookBaseUrl(guidebook: UUID?): URL? {
            return (select (GuideBook_Table.baseUrl) from GuideBook::class where(GuideBook_Table.uuid eq guidebook)).result?.baseUrl
        }


        internal fun deleteSelected(guidebooks: Collection<String>): Int {
            var result = 0
            (select from GuideBook::class).list
                    .filter { it.uuid.toString() in guidebooks }
                    .processTransaction { guideBook, databaseWrapper ->
                        guideBook.routes?.forEach { route ->
                            delete()
                                    .from(GuideRoutePoi::class.java)
                                    .where(GuideRoutePoi_Table.route_uuid.eq(route.uuid))
                                    .execute(databaseWrapper)
                        }
                        guideBook.delete(databaseWrapper)
                        result = result.inc()
                    }
            if (!(select from GuideBook::class).hasData) {
                Singleton.resetCurrentCategories()
                delete().from(GuideCategory::class)
            }
            if (result > 0) {
                Singleton.resetCurrentGuidebook()
            }
            return result
        }
    }
}
