package eu.ciastek.b.mobiguide

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import java.util.*

/**
 * List adapter with checkboxes ald small icons for Categories

 * @author Bogusław Ciastek
 * @version 1.0
 */
internal class MapWithThumbnailsAdapter(cxt: Context, private val mAllItemsData: Map<String, ListElem>)
    : BaseAdapter(), Filterable {

    data class ListElem(
            internal var label: String = String(),
            internal var thumbnail: Drawable? = null,
            internal var checked: Boolean = false
    )

    private val mFilter = CategoriesListFilter()
    private val mInflater = LayoutInflater.from(cxt)

    private val mFilteredData = mAllItemsData.toMutableMap()
    private val mKeys = mAllItemsData.keys

    operator fun iterator() = mFilteredData.iterator()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var mConvertView = convertView
        val holder: ViewHolder

        if (mConvertView is View) {
            holder = mConvertView.tag as ViewHolder
        } else {
            mConvertView = mInflater.inflate(R.layout.list_multiselect_with_thumbnails, parent, false)

            holder = ViewHolder().apply {
                checkBox = mConvertView?.findViewById(R.id.checkBoxItem)
                image = mConvertView?.findViewById(R.id.imageThumbnail)
            }
            mConvertView?.tag = holder
        }

        val item = mFilteredData[mKeys.elementAt(position)]
        holder.checkBox?.text = item?.label
        holder.checkBox?.isChecked = item?.checked ?: false
        holder.checkBox?.setOnClickListener {
            val isChecked = holder.checkBox?.isChecked ?: false
            item?.checked = isChecked
        }
        holder.image?.setImageDrawable(item?.thumbnail)
        return mConvertView
    }


    internal class ViewHolder {
        internal var image: ImageView? = null
        internal var checkBox: CheckBox? = null
    }


    override fun getCount(): Int = mFilteredData.size


    override fun getItem(position: Int): ListElem? = mFilteredData[mKeys.elementAt(position)]


    @Suppress("unused")
    internal fun getKey(position: Int): String? = mKeys.elementAtOrNull(position)


    override fun getItemId(position: Int): Long = position.toLong()


    override fun getFilter(): Filter = mFilter


    private inner class CategoriesListFilter : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filterString = constraint.toString().toLowerCase(Locale.getDefault())
            val results = FilterResults()

            if (constraint.toString().isNotEmpty()) {
                val rList = mAllItemsData.filter { it.value.label.toLowerCase(Locale.getDefault()).contains(filterString) }
                results.values = rList
                results.count = rList.size
            } else {
                synchronized(this) {
                    results.values = mAllItemsData
                    results.count = mAllItemsData.size
                }
            }
            return results
        }

        @Suppress("unchecked_cast")
        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            mFilteredData.clear()
            if (results is FilterResults) {
                mFilteredData.putAll(results.values as Map<String, ListElem>)
            }
            notifyDataSetChanged()
        }
    }
}
