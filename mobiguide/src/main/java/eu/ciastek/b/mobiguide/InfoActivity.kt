package eu.ciastek.b.mobiguide

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.ListView
import eu.ciastek.b.mobiguide.Singleton.currentGuidebook
import java.util.*


internal class InfoActivity : GuideActivity() {
    private val mPagesArray: ArrayList<GuidePage> = ArrayList()
    private var mListAdapter: GuideDbListAdapter<GuidePage>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentViewWithToolbar(R.layout.activity_list, true)

        findViewById<EditText>(R.id.inputSearch)?.apply {
            addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    mListAdapter?.filter?.filter(s.toString())
                }

                override fun afterTextChanged(s: Editable?) {}
            })
        }
        mListAdapter = GuideDbListAdapter(this, mPagesArray)
        findViewById<ListView>(R.id.listDbObjects)?.apply {
            adapter = mListAdapter
            setOnItemClickListener { adapterView, _, position, _ ->
                val p = adapterView.getItemAtPosition(position)
                if (p is GuidePage) {
                    val i = Intent(this@InfoActivity, DescriptionActivity::class.java)
                    i.putExtra(GuidePage::class.java.canonicalName.orEmpty(), p.uuid)
                    startActivity(i)
                }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        setListItems()
    }


    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        super.onSharedPreferenceChanged(sharedPreferences, key)
        if (key in arrayOf(
                getString(R.string.pref_filtered_categories_key),
                getString(R.string.pref_guidebook_key)
        )) {
            setListItems()
        }
    }


    private fun setListItems() {
        val fTags = getFilteredCategoryTagsSet
        mPagesArray.clear()
        mPagesArray.addAll(currentGuidebook.pages.orEmpty().filter { !it.isCategoryFiltered(this, fTags) })
        mListAdapter?.notifyDataSetChanged()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.action_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menu?.addFilterIcon()
        menu?.addSettings(true)
        return true
    }
}