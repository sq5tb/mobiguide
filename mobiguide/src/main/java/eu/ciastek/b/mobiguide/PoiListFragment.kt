package eu.ciastek.b.mobiguide

import android.content.*
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.ListView
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import eu.ciastek.b.mobiguide.Singleton.currentGuidebook

typealias OnPoiListClickListener = (AdapterView<*>?, View?, Int, Long) -> Unit

internal class PoiListFragment : Fragment(), SharedPreferences.OnSharedPreferenceChangeListener {
    private val updateListThread = Thread()

    private val mFilteredCategoryTags = mutableSetOf<String>()
    private val mPoiList = mutableListOf<GuidePoi>()
    private val mPoiListFiltered = mutableListOf<GuidePoi>()

    private var onPoiListClickListener: OnPoiListClickListener = { parent, view, position, _ ->
        val p = parent?.getItemAtPosition(position)
        if (view is View && p is GuidePoi) {
            val i = Intent(view.context, DescriptionActivity::class.java)
            i.putExtra(GuidePoi::class.java.canonicalName.orEmpty(), p.uuid)
            view.context.startActivity(i)
        }
    }

    private var mListAdapter: GuideDbListAdapter<GuidePoi>? = null
    private var mObject: GuideDbObject? = null
    private var mCurrentRoutePoi: GuidePoi? = null

    private var distanceSort = false

    private var mReceiver = LocationReceiver()
    private var mService: GuideLocationService? = null
    private var mServiceIsConnected = false
    private val guideLocationConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as GuideLocationService.GuideLocationBinder
            mService = binder.service
            this@PoiListFragment.updateListOnChangePosition(mService?.lastKnownLocation)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            mService = null
        }
    }


    fun setPoiListClickListener(listener: OnPoiListClickListener) {
        onPoiListClickListener = listener
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)

        context?.let {
            mListAdapter = GuideDbListAdapter(it, mPoiListFiltered)
            view?.findViewById<ListView>(R.id.listDbObjects)?.apply {
                adapter = mListAdapter
                setOnItemClickListener { adapterView, v, i, l ->
                    onPoiListClickListener(adapterView, v, i, l)
                }
            }
            view?.findViewById<EditText>(R.id.inputSearch)?.apply {
                addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                        mListAdapter?.filter?.filter(s.toString())
                    }

                    override fun afterTextChanged(s: Editable?) {}
                })
            }
        }
        return view
    }


    override fun onResume() {
        super.onResume()
        mFilteredCategoryTags.clear()
        context?.let { c ->
            PreferenceManager.getDefaultSharedPreferences(c)
                .registerOnSharedPreferenceChangeListener(this)
            if (!mServiceIsConnected) {
                c.bindService(
                    Intent(c, GuideLocationService::class.java),
                        guideLocationConnection, Context.BIND_AUTO_CREATE)
                c.registerReceiver(mReceiver, IntentFilter(GuideLocationService.BROADCAST_ACTION))
                mServiceIsConnected = true
            }
            mFilteredCategoryTags.addAll(c.getFilteredCategoryTagsSet)
        }
        loadListItems(mObject)
        updateListOnChangePosition(mService?.lastKnownLocation)
    }


    override fun onPause() {
        super.onPause()
        context?.let { c ->
            PreferenceManager.getDefaultSharedPreferences(c)
                .unregisterOnSharedPreferenceChangeListener(this)
            if (mServiceIsConnected) {
                c.unregisterReceiver(mReceiver)
                c.unbindService(guideLocationConnection)
                mServiceIsConnected = false
            }
        }
    }


    internal fun setObject(obj: GuideDbObject?, routePoi: GuidePoi? = null) {
        mObject = obj
        mCurrentRoutePoi = routePoi
    }


    private fun loadListItems(o: GuideDbObject?) {
        when (o) {
            is GuidePoi -> {
                val position = o.coordinates
                if (position?.isValid == true) {
                    context?.let {c ->
                        val maxNum = PreferenceManager.getDefaultSharedPreferences(c)
                            .getString(getString(R.string.pref_near_poi_max_number_key), String())?.toIntOrNull() ?: resources.getInteger(R.integer.pref_near_poi_max_number_default)

                        val maxDist = PreferenceManager.getDefaultSharedPreferences(c)
                            .getString(getString(R.string.pref_near_poi_max_distance_key), String())?.toIntOrNull() ?: resources.getInteger(R.integer.pref_near_poi_max_distance_default)

                        val nearPoints = c.currentGuidebook.poiWithinRadius(position, maxDist.toDouble(), maxNum, c)
                        distanceSort = false
                        setListItems(nearPoints)
                    }
                }
            }
            is GuideRoute -> {
                distanceSort = false
                setListItems(o.poi.orEmpty())
            }
            else -> filterListItems()
        }
    }

    private fun filterListItems() {
        mPoiListFiltered.clear()
        mPoiListFiltered.addAll(mPoiList.filter { !it.isCategoryFiltered(context, mFilteredCategoryTags) })
        mListAdapter?.notifyDataSetChanged()
    }


    internal fun setListItems(poi: List<GuidePoi>) {
        mPoiList.clear()
        mPoiList.addAll(poi)
        filterListItems()
    }

    internal fun setDistanceSort(sort: Boolean) {
        if (distanceSort != sort) {
            distanceSort = sort
            mListAdapter?.notifyDataSetChanged()
        }
    }

    override fun onSharedPreferenceChanged(preferences: SharedPreferences?, key: String?) {
        if (key == getString(R.string.pref_filtered_categories_key)) {
            mFilteredCategoryTags.clear()
            mFilteredCategoryTags.addAll(context.getFilteredCategoryTagsSet)
        }
        if (key in arrayOf(
                R.string.pref_near_poi_max_distance_key,
                R.string.pref_near_poi_max_number_key,
                R.string.pref_filtered_categories_key).map { getString(it) }) {
            loadListItems(mObject)
        }
    }

    private fun updateListOnChangePosition(loc: Location?) {
        if (updateListThread.isAlive) {
            return
        }
        updateListThread.run {
            if (mListAdapter is GuideDbListAdapter && loc is Location) {
                mListAdapter?.forEach {
                    it.updateBearingAndDistance(loc)
                }
                if (distanceSort) {
                    mListAdapter?.sort(GuidePoi.DistanceComparator(LatLong(loc.latitude, loc.longitude)))
                }
                mListAdapter?.notifyDataSetChanged()
            }
        }
    }


    private inner class LocationReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent is Intent && intent.action == GuideLocationService.BROADCAST_ACTION) {
                val loc = intent.getParcelableExtra<Location>(GuideLocationService.KEY_LOCATION)
                this@PoiListFragment.updateListOnChangePosition(loc)
            }
        }
    }
}
