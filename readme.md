# Praca dyplomowa inżynierska

napisana na

### Wydziale Matematyczno-Informatycznym
## Uniwersytetu Technologiczno-Humanistycznego im. Kazimierza Pułaskiego w Radomiu


Autor: **Bogusław Ciastek**  
Promotor: **dr Artur Bartoszewski**

Radom, 2018

***


# Engineer's thesis

## Kazimierz Pulaski University of Technology and Humanities in Radom
### Faculty of Informatics and Mathematics

Author: **Bogusław Ciastek**  
Thesis advisor: **Artur Bartoszewski, PhD**

Radom, Poland, 2018
